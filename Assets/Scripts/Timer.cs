﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {

	public float time = 0.0f;
	public bool start = false;

	
	// Update is called once per frame
	void Update () {
	
		if (start) {
			time += Time.deltaTime;
		}
	}
}

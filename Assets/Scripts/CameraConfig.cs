﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CameraConfig : MonoBehaviour {

	public void SetCameraBeforeBattle(BattleField2 battlefield)
	{
        Tool.FreezePlayer (true);
		Destroy (Tool.GetCameraFollow());
		GameObject cameraObject = Tool.FindCamera ();
		Transform t = battlefield.transform;
		cameraObject.transform.position = new Vector3 (t.position.x + 7, t.position.y + 4, cameraObject.transform.position.z);
		Debug.Log ("x: " + cameraObject.transform.position.x +" y: " + (cameraObject.transform.position.y - 10) + " z: " + cameraObject.transform.position.z + " width: " + Camera.main.pixelWidth + " height: " + Camera.main.pixelHeight);
		Camera.main.orthographic = true; 
		Camera.main.orthographicSize = 5;
	}
	public void RestoreCamera()
	{
		GameObject cam = Tool.FindCamera ();
		cam.AddComponent<CameraFollow> ();
		Tool.GetCameraFollow ().SetTarget(gameObject);
		Camera.main.orthographicSize = 3;
        Tool.FreezePlayer (false);
	}

	public void SetConversationCamera()
	{

      GameObject cameraObject = Tool.FindCamera ();
      Destroy (Tool.GetCameraFollow());
      Transform t = cameraObject.transform;


        Tool.FindMe().frozenPoz = Tool.FindMe().gameObject.transform.position;
      cameraObject.transform.position = new Vector3 (t.position.x + 100, t.position.y + 100, cameraObject.transform.position.z);
       Tool.FreezePlayer(true);
    }
}

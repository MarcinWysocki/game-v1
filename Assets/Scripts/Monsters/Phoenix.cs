﻿using System;
using System.Collections.Generic;

public class Phoenix : _Monster
{

    public Phoenix(int lvl)
    {
        name = "Phoenix";
        type = "Fire";
        level = lvl;
        maxHP = 16 * lvl;
        currentHP = maxHP;
        speed = 11;
        actionPoints = 9;
        currentAP = actionPoints;

        attackList = new List<Attack>();
        SetFirstAttack();
        SetExeprienceOnStart(level);
    }
    
    public override List<Attack> SetFutureAttack()
    {
        List<Attack> list = new List<Attack>();
        list.Add(new Punch());
        list.Add(new Bite());
        list.Add(new Tackle());
        list.Add(new Bite());
        return list;
    }

    public override List<int> SetPromotionLVL()
    {
        List<int> list = new List<int>();
        list.Add(6);
        list.Add(8);
        list.Add(10);
        list.Add(12);
        return list;
    }
}

﻿using System.Collections.Generic;

public class Worm : _Monster{

	public Worm(int lvl)
	{
		name = "Worm";
		type = "Normal";
		level = lvl;
		maxHP = 5 * lvl;
		currentHP = maxHP;
		speed = 1;
		actionPoints = 3;
		currentAP = actionPoints;

		attackList = new List<Attack> ();
        SetFirstAttack();
        SetExeprienceOnStart (level);
	}

    override
    public List<Attack> SetFutureAttack()
    {
        List<Attack> list = new List<Attack>();
        list.Add(new Tackle());
        list.Add(new Tackle());
        list.Add(new Tackle());
        list.Add(new Tackle());
        return list;
    }
    public override List<int> SetPromotionLVL()
    {
        List<int> list = new List<int>();
        list.Add(6);
        list.Add(8);
        list.Add(10);
        list.Add(12);
        return list;
    }
}

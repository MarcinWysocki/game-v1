﻿using System.Collections.Generic;

public class Angel : _Monster
{
    public Angel(int lvl)
    {
        name = "Angel";
        type = "Psychic";
        level = lvl;
        maxHP = 15 * lvl;
        currentHP = maxHP;
        speed = 10;
        actionPoints = 9;
        currentAP = actionPoints;

        attackList = new List<Attack>();
        SetFirstAttack();
        SetExeprienceOnStart(level);

    }

    override
    public List<Attack> SetFutureAttack()
    {
        List<Attack> list = new List<Attack>();
        list.Add(new Punch());
        list.Add(new Punch());
        list.Add(new Punch());
        list.Add(new Punch());
        return list;
    }
    public override List<int> SetPromotionLVL()
    {
        List<int> list = new List<int>();
        list.Add(6);
        list.Add(8);
        list.Add(10);
        list.Add(12);
        return list;
    }
}

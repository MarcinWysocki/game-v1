﻿using UnityEngine;
using System.Collections.Generic;

public abstract class _Monster : IAttack
{
    public  string name;
    public string type;
    public int level;
    public int experience;
    public int experienceToNextLevel;
    public int maxHP;
    public int currentHP;
    protected int speed;
    public int actionPoints;
    protected int currentAP;

    public List<Attack> attackList = new List<Attack>();
    protected List<Attack> futureAttacks;
    protected List<int> promotionLVL;

    public bool moveDone = false;
    public bool canMove = true;
    public bool dead = false;


    public void AddExperience(int exp)
    {
        experience += exp;
    }

    public bool CheckLevel()
    {   //Sprawdza czy monster osiągnął nowy poziom poprzez porównanie oczekiwanego exp do wymaganego
        bool lvlUp = false;
        if (experience >= experienceToNextLevel)
        {
            level++;
            //Ustawia nowe wymagania exp do zdobycia nowego poziomu
            SetNewExperienceLevel();
            Debug.Log("Congratulation, level up: " + level);
            //Ustawia flagę, że został nowy poziom został zdobyty
            lvlUp = true;
            //Sprawdza czy na nowym poziomie przysługuje mu nowy atak.
            if (IsNewAttackTime())
            {
                SetNewAttack();
                RemovePromotionLVL();
            }

        }
        //Sprawdzenie czy został zdobyty tylko jeden poziom poprzez rekurencyjne wywołanie metody do sprawdzania ale tylko w przypadku wcześniej zdobytego poziomu.
        if (lvlUp)
        {
            CheckLevel();
        }
        Debug.Log("Exp: " + experience + "/" + experienceToNextLevel);
        return lvlUp;
    }

    public int GetCurrentAP()
    {
        return currentAP;
    }
    public int GetDamage(Attack attack)
    {
        return attack.GetPower() * level;
    }
    private Attack GetFirstAttack()
    {   //Zwraca pierwszy atak z listy oczekujących ataków
        return futureAttacks[0];
    }
    private int GetPromotionLVL()
    { return promotionLVL[0]; }
    public int GetSpeed()
    {
        return speed;
    }

    private bool IsNewAttackTime()
    {
        //Sprawca czy na tym poziomie przysługuje premia
        return this.level == promotionLVL[0];
    }

    public void ReduceCurrentAP(int ap)
    {
        currentAP -= ap;
    }
    public void ReduceCurrentHP(int hp)
    {
        currentHP -= hp;
    }

    protected void SetFirstAttack()
    {
        //Ustawia listę oczekiwanych ataków przy kolejnych awansach. Każda klasa potwora ma swoje przeciążenie tej metody
        futureAttacks = SetFutureAttack();
        //Ustawia pierwszy dostępny atak
        SetNewAttack();
        //Ustawia listę poziomów na których monster może otrzymać nowy atak. Każda klasa potwora ma swoje przeciążenie tej metody
        promotionLVL = SetPromotionLVL();
    }
    public void SetCurrentAP(int ap)
    {
        currentAP = ap;
    }
    private void SetNewAttack()
    {
        //Dodaje do listy ataków pierwszy z listy oczekujących
        attackList.Add(GetFirstAttack());
        //Dodany atak zostaje usunięty z listy oczekujących, aby nie został dodany po raz drugi.
        RemoveAddedAttack();
    }
    private void SetNewExperienceLevel()
    {
        experienceToNextLevel += 10 * level;
    }

    protected void RemoveAddedAttack()
    {
        //Usuwa atak z listy oczekiwanych ataków. (Wywoływane podczas zdobycia nowego ataku)
        if (futureAttacks.Count > 0)
        { futureAttacks.RemoveAt(0); }
        else { Debug.Log("It was last available attack."); }
    }
    protected void RemovePromotionLVL()
    {
        //Usuwa numer poziomu z listy oczekiwanych poziomów. (Wywoływane podczas zdobycia nowego ataku)
        if (promotionLVL.Count > 0)
        { promotionLVL.RemoveAt(0); }
        else { Debug.Log("It was last available promotion."); }
    }

    protected void SetExeprienceOnStart(int lvl)
    {
        //Ustawia początkowe doświadczenie dla nowo zdobytego potwora z danym poziomem
        //!!!Metoda mało generyczna i na daną chwilę nie nadaje się dla różnych typów
        experience = 0;
        experienceToNextLevel = 10;
        for (int i = 1; i <= lvl; i++)
        {
            experienceToNextLevel += 10 * i;
        }
    }

    //Metody implementujące interfej IAttack. Ciała metody są w klasach poszczególnych potworów.
    public abstract List<Attack> SetFutureAttack();
    public abstract List<int> SetPromotionLVL();


}

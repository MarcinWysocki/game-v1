﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class CameraFollow : MonoBehaviour {

    public GameObject target;
    private Transform targetTransform;

    // Use this for initialization
    void Start()
    {
            Player p = Tool.FindMe();
            targetTransform = p.gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (targetTransform == null)
        {
            transform.position = new Vector3();
        }
        else
        {
            transform.position = new Vector3(targetTransform.position.x, targetTransform.position.y, transform.position.z);
        }
    }
	public void SetTarget(GameObject t)
	{
		target = t;
	}
}

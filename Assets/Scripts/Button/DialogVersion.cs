﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DialogVersion : MonoBehaviour {

    public int keyChoice;
    private void OnLevelWasLoaded(int level)
    {
        if (level != 0)
        GetComponent<Button>().onClick.AddListener(delegate { SelectDialog(); });
    }

    void OnGUI()
    {
        if (Event.current.Equals(Event.KeyboardEvent(keyChoice.ToString())))
        {
            SelectDialog();
        }
    }

    
    public void SelectDialog()
    {
        // Debug.Log("Select dialog with mission: " + Tool.FindMe().currentMissionIndex); 
        Response respons = DialogManager.dialogTable[Tool.FindMe().talkIndex, keyChoice];
        //Debug.Log("Talk turn: " + Tool.FindMe().talkIndex + " Finish: " + respons.isfinish);
        if (respons.isfinish)
        { Exit(); }
        else
        {
            Tool.GetMission().respect += respons.respect;
            Tool.FindMe().talkIndex += respons.respect >= 0 ? 1 : 2;
            DialogManager.UpdateTalkBar(Tool.FindMe().talkIndex);
            respons = DialogManager.dialogTable[Tool.FindMe().talkIndex, keyChoice];
        }

 
    }

    public void Exit()
    {
        Debug.Log("Exit from conversation");
        Tool.FindMe().ResetMissionIndex();
        Tool.FreezePlayer(false);
        SceneManager.LoadScene(Tool.FindMe().currentSceneIndex);
        Tool.FindMe().ResetPosition();
        GameObject cam = Tool.FindCamera();
        cam.AddComponent<CameraFollow>();
        Tool.GetCameraFollow().SetTarget(gameObject);
    }

    public void SetText(string text)
    {
        gameObject.GetComponentInChildren<Text>().text = text;
    }
    public string GetText()
    { return gameObject.GetComponentInChildren<Text>().text; }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndTalk : MonoBehaviour {

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(delegate () { ContinueOrExit(); });
    }

    void OnGUI()
    {
        if (Event.current.Equals(Event.KeyboardEvent("return")))
        {
            Debug.Log("Key: " + Event.current);
            ContinueOrExit();
        }
    }

    public void ContinueOrExit()
    {
        Debug.Log("Exit from conversation");
        Tool.FreezePlayer(false);
        SceneManager.LoadScene(Tool.FindMe().currentSceneIndex);
        Tool.FindMe().transform.position = Tool.FindMe().frozenPoz;
        GameObject cam = Tool.FindCamera();
        cam.AddComponent<CameraFollow>();
        Tool.GetCameraFollow().SetTarget(gameObject);

    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Chest : MonoBehaviour {

	public int coins;
	bool open = false;
	Animator animator;
	public string[] items;

	void Start () {
		animator = GetComponent<Animator> ();
	}

	void OnCollisionEnter2D(Collision2D target) {
		if (!open) {
			open = true;
			animator.SetInteger ("AnimState", 1);
			Bag bag = Tool.FindMe ().GetComponent<Bag> ();
			bag.AddCash (coins);
			foreach (string stuffName in items) {
				bag.addStuff(System.Reflection.Assembly.GetExecutingAssembly ().CreateInstance (stuffName) as Item);
			}
			Debug.Log(Tool.FindMe ().GetComponent<Bag> ().getItems().Count);
		}

	}
}

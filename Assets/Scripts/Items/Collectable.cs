﻿using UnityEngine;
using System.Collections;

public class Collectable : MonoBehaviour {

	public int coinValue;

	void OnTriggerEnter2D(Collider2D target){
		if (target.gameObject.tag == "Player") {
			Tool.FindMe ().GetComponent<Bag> ().AddCash (coinValue);
			Destroy (gameObject);
		}
	}
}

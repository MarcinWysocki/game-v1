﻿using UnityEngine;
using System.Collections.Generic;

public class SearchPath {

	public Graph graph;
	public List<Tile> reachable;
	public List<Tile> explored;
	public List<Tile> path;
	public Tile goalNode;
	public int iterations;
	public bool finished;
	private Tile[,] tiles;

	public SearchPath(Graph graph, Tile[,] t){
		this.graph = graph;
		tiles = t;
	}

	public void Start(Tile start, Tile goal){
		reachable = new List<Tile> ();
		reachable.Add (start);

		goalNode = goal;

		Debug.Log ("Start node: " + start.id + " goal: " + goalNode.id);
		explored = new List<Tile> ();
		path = new List<Tile> ();
		iterations = 0;
	}

	public void Step(){
		if (path.Count > 0)
			return;

		Debug.Log ("reachable count: " + reachable.Count);
		if (reachable.Count == 0) {
			finished = true;
			return;
		}


		iterations ++;

		Tile node = ChoseNode ();
		if (node == goalNode) {
			while(node != null){
				path.Insert(0, node);
				node = node.previous;
			}
			finished = true;
			return;
		}

		reachable.Remove (node);
		explored.Add (node);

		Debug.Log ("Adjecents for: " + node.id + " = " + node.adjecent.Count);
		for (var i = 0; i < node.adjecent.Count; i++) {
			AddAdjacent(node, node.adjecent[i]);
		}
	}

	private bool findNodeResult(Tile node, Tile adjacent)
	{
		return FindNode (adjacent, explored) || FindNode (adjacent, reachable);
	}
	public void AddAdjacent(Tile node, Tile adjacent){

		bool findNoderesult = findNodeResult (node, adjacent);
		Debug.Log ("Find node result: " + findNoderesult);
		if (findNoderesult) {
			return;
		}

		adjacent.previous = node;
		reachable.Add (adjacent);
	}

	public bool FindNode(Tile node, List<Tile> list){
		return GetNodeIndex(node, list) >= 0;
	}

	public int GetNodeIndex(Tile node, List<Tile> list){
		for (var i = 0; i < list.Count; i++) {
			if(node == list[i]){
				return i;
			}
		}

		return -1;
	}

	public Tile ChoseNode(){
		return reachable [Random.Range (0, reachable.Count)];
	}

}

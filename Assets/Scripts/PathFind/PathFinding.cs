﻿using UnityEngine;
using System.Collections;

public class PathFinding : MonoBehaviour {

	private Tile[,] battlefield;
	public SearchPath search;
	public void FindPath(Tile startTile, Tile endTile, Tile[,] field) {
		battlefield = field;

		Graph graph = new Graph (startTile, battlefield);
		Debug.Log ("Graph length: " + battlefield.Length);

		search = new SearchPath (graph, battlefield);
		search.Start (startTile, endTile);

		while (!search.finished) {
			search.Step();
		}

		Debug.Log ("Search done. Path length " + search.path.Count + " iterations " + search.iterations);

	//	ResetMapGroup (graph);

		//foreach (Tile t in search.path) {
		//	t.GetComponent<SpriteRenderer> ().sprite = GameObject.FindGameObjectWithTag ("BattleF2").GetComponent<BattleField2> ().markedTile.GetComponent<SpriteRenderer> ().sprite;
		//	//t.transform.position = new Vector3 (t.transform.position.x , t.transform.position.y, t.transform.position.z);
		//	//Destroy (t.gameObject);
		//}


	}

	public void displayPath()
	{
		string text = "Path: ";
		foreach (Tile t in search.path) {
			text += t.id + " ";
		}
		Debug.Log (text);
	}



	void ResetMapGroup(Graph graph){
		foreach (Tile t in battlefield) {
			t.transform.position = new Vector3 (t.transform.position.x + 1, t.transform.position.y, t.transform.position.z);
		}
	}
}

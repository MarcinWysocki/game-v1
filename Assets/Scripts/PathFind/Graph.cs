﻿using UnityEngine;
using System.Collections;
using System;

public class Graph{

	public int rows = 0;
	public int cols = 0;

	public Graph(Tile start, Tile[,] tiles)
	{
		//Użyć tablicy z parametru bez tworzenia nowej
		rows = tiles.GetLength (0);
		cols = tiles.GetLength (1);
		Debug.Log ("Tile size: " + tiles.Length + " rows: " + rows + " cols: " + cols);

		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				tiles [r, c].ClearPrev ();

				Debug.Log("id= " + tiles [r, c].id + "Start id: " + start.id);
				if (tiles [r, c].GetMonster () != null && tiles [r, c].id != start.id) {
					Debug.Log("T id: " + tiles [r, c].id + "Start id: " + start.id);
					continue;
				}

				// Up
				if(r > 0){
					tiles [r,c].adjecent.Add (tiles [r - 1, c]);
				}

				// Right
				if (c < cols - 1) {
					tiles [r,c].adjecent.Add (tiles [r, c + 1]);
				}

				// Down
				if (r < rows - 1) {
					tiles [r,c].adjecent.Add (tiles [r + 1, c]);
				}

				// Left
				if (c > 0) {
					tiles [r,c].adjecent.Add (tiles [r, c - 1]);
				}
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class MyPathFinding : MonoBehaviour {

	enum DIRECTION {RIGHT, LEFT, UP, DOWN};
	string direction;

	private Tile[,] battlefield;
	private Vector2 defaultDirection;
	private Vector2 startAddress;
	private Vector2 currentAddress;
	private Vector2 endAddress;
	private Vector2 remainDistance;
	public List<Tile> path;
	private bool vertical = false;
	private bool isDirectionSet = false;
	int counter = 0;
	public void FindPath(Tile start, Tile end, Tile[,] field)
	{
		path = new List<Tile> ();
		battlefield = field;
		startAddress = start.GetAdress ();
		endAddress = end.GetAdress ();
		currentAddress = startAddress;
		defaultDirection = endAddress - startAddress;
		remainDistance = endAddress - currentAddress;
		setDirection ();
		Debug.Log ("Direction: " + defaultDirection.x + "," + defaultDirection.y + " " + direction);
		path.Add (start);

		goToDirection ();
		goToDirection ();
		Debug.Log ("Goal: " + remainDistance.Equals (new Vector2 (0, 0)) + " distance: " + remainDistance);
		displayPath ();
	}


	private void goToDirection()
	{
		if (!isDirectionSet) {
			setDirection ();
		}
		Vector2 move = new Vector2();
		switch (direction) {
		case "RIGHT":
			move = new Vector2 (0, 1);
			vertical = false;
			break;
		case "LEFT":
			move = new Vector2 (0, -1);
			vertical = false;
			break;
		case "UP":
			move = new Vector2 (1, 0);
			vertical = true;
			break;
		case "DOWN":
			move = new Vector2 (-1, 0);
			vertical = true;
			break;
		default:
			break;
		}
		goToThrough (move);
		remainDistance = endAddress - currentAddress;
		Debug.Log ("Remain path distance: " + remainDistance.x + "," + remainDistance.y);
	}

	private void goToThrough(Vector2 move)
	{
		float remain = vertical ? remainDistance.x : remainDistance.y;
		remain = Mathf.Abs (remain);
		Debug.Log ("Current Address: " + currentAddress.x + "x" + currentAddress.y + " remain in loop: " + remain);
		for (int i = 1; i <= remain; i++) {

			if (isFieldEmpty(move)) {
				setNextTile (move);
				currentAddress = new Vector2 ((int)(currentAddress.x + move.x), (int)(currentAddress.y + move.y)); 
				path.Add (battlefield [(int)currentAddress.x, (int)currentAddress.y]);
				Debug.Log ("New path Address: " + currentAddress.x + "x" + currentAddress.y);
			} else if (isFieldGoal (move, endAddress)) {
				setNextTile (move);
				currentAddress = new Vector2 ((int)(currentAddress.x + move.x), (int)(currentAddress.y + move.y));
				path.Add (battlefield [(int)currentAddress.x, (int)currentAddress.y]);
				return;
			} else {
				Vector2 notEmpty = battlefield [(int)(currentAddress.x + move.x), (int)(currentAddress.y + move.y)].GetAdress ();
				Debug.Log (notEmpty.x + "x" + notEmpty.y + " is not empty field");
				break;
			}
		}
	}

	private void setNextTile(Vector2 move)
	{
		battlefield [(int)currentAddress.x, (int)currentAddress.y].nextTile = battlefield [(int)(currentAddress.x + move.x), (int)(currentAddress.y + move.y)];
	}
	private bool isFieldEmpty(Vector2 move)
	{
		return battlefield [(int)(currentAddress.x + move.x), (int)(currentAddress.y + move.y)].GetMonster () == null;
	}
	private bool isFieldGoal(Vector2 move, Vector2 end)
	{
		return battlefield [(int)(currentAddress.x + move.x), (int)(currentAddress.y + move.y)].GetAdress ().Equals (end);
	}

	private void setDirection()
	{
		if (remainDistance.y > 0 && remainDistance.x >= 0)
		{
			direction = DIRECTION.RIGHT.ToString();
		}
		if (remainDistance.y >= 0 && remainDistance.x < 0)
		{
			direction = DIRECTION.DOWN.ToString();
		}
		if (remainDistance.y < 0 && remainDistance.x <= 0)
		{
			direction = DIRECTION.LEFT.ToString();
		}
		if (remainDistance.y <= 0 && remainDistance.x > 0)
		{
			direction = DIRECTION.UP.ToString();
		}
	}

	public void displayPath()
	{
		string text = "Path: ";
		foreach (Tile x in path) {
			text += x.GetAdress ().x + "x" + x.GetAdress ().y + " ";
		}
		Debug.Log (text);
	}
	public List<Tile> GetPath()
	{
		return path;
	}



}

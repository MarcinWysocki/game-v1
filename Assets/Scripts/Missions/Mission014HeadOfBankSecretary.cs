﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class Mission014HeadOfBankSecretary : IMission
{
    public override void StartMission(string Name)
    {
        Tool.FindMe().currentMissionIndex = missionID;
        if (!isStarted)
        {
            isStarted = true;
            Tool.FindMe().talkIndex = 0;
            Tool.FindMe().StartTalk();
        }
        CheckStatus();
        
    }
    public override void CheckStatus()
    {
        IMission mission = Tool.GetStory().missions[13];
        if (mission.isStarted)
        {
            isDone = true;
            EndMission();
        }
        else
        {
            Tool.FindMe().talkIndex = 0;
            Tool.FindMe().StartTalk();
        }
    }

    public override bool EndMission()
    {
        if (isDone && isFinished)
        {
            Tool.FindMe().talkIndex = 2;
            Tool.FindMe().StartTalk();
        }
        if (isDone && !isFinished)
        {
            move();
            Tool.FindMe().talkIndex = 1;
            Tool.FindMe().StartTalk();
            isFinished = true;
        }
        return true;
    }
    private void move()
    {
        hero = Tool.FindMyMissionPerson(Person.Story.HEAD_BANK_SECRETARY);
        Vector3 position = hero.transform.position;
        hero.transform.position = new Vector3(position.x, position.y + 2, position.z);
    }
}


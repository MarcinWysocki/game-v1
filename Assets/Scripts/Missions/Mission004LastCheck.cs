﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Missions;

public class Mission004LastCheck : IMission {

	public override void StartMission(string name)
	{
        trainer = Tool.FindTrainer(Person.Trainer.MARC);
        Tool.FindMe().currentMissionIndex = missionID;
        expForMission = 25;
        if (!isStarted) {
			isStarted = true;
            Tool.FindMe().talkIndex = 0;
            firstMove();
            Tool.FindMe().StartTalk();
		} else {
			CheckStatus ();
		}
	}
	public override void CheckStatus()
	{
		if (trainer.IsDefeated) {
			isDone = true;
			EndMission ();
		} else {
            Tool.FindMe().talkIndex = 1;
            Tool.FindMe().StartTalk();
        }
	}
	public override bool EndMission()
	{
		if (isDone && isFinished) {
            Tool.FindMe().talkIndex = 3;
            Tool.FindMe().StartTalk();
        }
		if (isDone && !isFinished) {

            BattleAfter batttleAfter = Tool.FindMe().GetComponent<BattleAfter>();
            batttleAfter.AddExperience (expForMission);
            speach = Communicates.ReceiveCommunicate(expForMission, batttleAfter.lvlCommunicate);
			secondMove ();
            isFinished = true;
            Tool.FindMe().talkIndex = 2;
            DialogManager.AddText(missionID, Tool.FindMe().talkIndex, 0, false, speach);
            Tool.GoToTalkScene();
        }
        return true;
	}

	private void firstMove(){
		hero = Tool.FindMyMissionPerson (Person.Story.UNCELSAM);
		Vector3 positionHero = hero.transform.position;
		hero.transform.position = new Vector3 (positionHero.x - 1.5f, positionHero.y, positionHero.z);
		Vector3 defaultScale = hero.transform.localScale;
	//	hero.transform.localScale = new Vector3 (defaultScale.x * (-1), defaultScale.y, defaultScale.z);
	}
	private void secondMove(){
        hero = Tool.FindMyMissionPerson(Person.Story.UNCELSAM);
        Vector3 positionHero = hero.transform.position;
		Vector3 positionTrainer = trainer.gameObject.transform.position;
		Vector3 scale = trainer.gameObject.transform.localScale;
		trainer.gameObject.transform.position = new Vector3 (positionTrainer.x, positionTrainer.y-1, positionTrainer.z);
		trainer.gameObject.transform.localScale = new Vector3 (scale.x * (-1), scale.y, scale.z);
	}

	private int getMatchingLevel()
	{
		int monsterLevel = Tool.FindMe().pocket.getCurrentPocket () [0].level;
		Debug.Log ("You monster: " + monsterLevel + ". Foe monster: " + (monsterLevel - 1));
		return monsterLevel - 1;
	}
}

﻿using UnityEngine;
using System.Collections;

using Assets.Scripts.Missions;

public class Mission001FirstMonsterChoice : IMission {

	FirstMonsterChoice[] choice;

    public override void StartMission(string name)
	{
		player = Tool.FindMe ();
		IMission mission0 = player.GetComponent<Story> ().missions [0];
		player.cameraConfig.SetConversationCamera ();
        Tool.FindMe().currentMissionIndex = missionID;
        if (mission0.isFinished)
        { 
            if (!isStarted) {
                Debug.Log("SELECT FRIEND");
                isStarted = true;
                Tool.FindMe().talkIndex = 0;
                Tool.FindMe().StartTalk();

            } else {
				CheckStatus ();
			}

		} else {
            Debug.Log("MISSION 0 IS NOT FINISHED");
            Tool.FindMe().talkIndex = 4;
            Tool.FindMe().StartTalk();
        }

	}
	public override void CheckStatus()
	{
        Debug.Log("CHECK STATUS");
        int pocketCount = player.pocket.NotEmptyCount ();
        if (1 == pocketCount) {
            isDone = true;
		} else {
            Debug.Log("Pocket count: " + pocketCount);
        }
		EndMission ();
    }
	public override bool EndMission()
	{
		if (!isDone && !isFinished) {
            Tool.FindMe().talkIndex = 2;
            DialogManager.UpdateTalkBar(Tool.FindMe().talkIndex);
        }
		if (isDone && isFinished) {
        
            Tool.FindMe().talkIndex = 3;
            //    DialogManager.UpdateTalkBar(Tool.FindMe().talkIndex);
            Tool.FindMe().StartTalk();
        }
		if (isDone && !isFinished) {
            Tool.FindMe().talkIndex = 1;
            introduceNewMonster();
            DialogManager.UpdateTalkBar(Tool.FindMe().talkIndex);
			isFinished = true;
        }
        return true;
	}

    private void introduceNewMonster()
    {
        _Monster newMonster = player.pocket.getCurrentPocket()[0];
        string name = newMonster.name;
        string lvl = newMonster.level.ToString();
        string text = Communicates.ReceiveCommunicate(player.pocket.getCurrentPocket()[0]);
        DialogManager.AddText(Tool.FindMe().talkIndex, 0, false, text);
    }


}

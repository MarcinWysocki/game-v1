﻿using UnityEngine;
using System.Collections;

public class MissionID : MonoBehaviour {

	public int ID = 0;
	public string name = "";
	public int experience = 0;
	public int prize = 0;
}

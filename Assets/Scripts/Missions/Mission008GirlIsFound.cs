﻿using UnityEngine;
using System.Collections;

public class Mission008GirlIsFound : IMission {

	public override void StartMission(string name)
	{
        IMission mission7 = Tool.FindMe().GetComponent<Story>().missions[7];
        Tool.FindMe().currentMissionIndex = missionID;
        if (mission7.isStarted)
        {
            if (!isStarted)
            {
                isStarted = true;
                Tool.FindMe().talkIndex = 1;
                Tool.FindMe().StartTalk(); 
            }
            CheckStatus();
        }
        else
        {
            Tool.FindMe().talkIndex = 0;
            Tool.FindMe().StartTalk();
        }
	}
	public override void CheckStatus()
	{
        isDone = true;
        EndMission();
    }
	public override bool EndMission()
	{
		if (isDone && isFinished) {
            Tool.FindMe().talkIndex = 3;
            Tool.FindMe().StartTalk();

        }
		if (isDone && !isFinished) {
            Tool.FindMe().talkIndex = 1;
            Tool.FindMe().StartTalk();
        }
        return true;
	}

}

﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Missions;

public class Mission002FirstMonsterDone : IMission {

    
    public override void StartMission(string name)
	{
		IMission mission1 = Tool.FindMe().GetComponent<Story> ().missions [1];
        Tool.FindMe().currentMissionIndex = missionID;
        prizeForMission = 10;
        Debug.Log("Talk index: " + Tool.FindMe().talkIndex + ". Mission: " + Tool.FindMe().currentMissionIndex);
        if (mission1.isFinished)
        {
            if (!isStarted)
            {
                isStarted = true;
                Tool.FindMe().talkIndex = 0;
                Tool.FindMe().StartTalk();
            }
            else
            { CheckStatus(); }
        }
        else
        {
            Tool.FindMe().talkIndex = 5;
            Tool.FindMe().StartTalk();
        }
	}
	public override void CheckStatus()
	{
		int monsterCount = Tool.FindMe().pocket.NotEmptyCount ();
		if (0 == monsterCount) {
            Tool.FindMe().talkIndex = 3;
            Debug.Log(UNREACHABLE + missionID);
		} else if (1 == monsterCount) {
			isDone = true;
		} else if (1 < monsterCount) {
            Debug.Log(UNREACHABLE + missionID);
        }
		EndMission ();
	}
	public override bool EndMission()
	{
		if (!isDone && !isFinished) {
            Debug.Log(UNREACHABLE + missionID);
        } else if (isDone && !isFinished) {
            Tool.FindMe().talkIndex = 2;
			Tool.FindMe().bag.AddCash (prizeForMission);
            DialogManager.AddText(missionID,Tool.FindMe().talkIndex, 0, false, Communicates.ReceiveCommunicate(prizeForMission));
            afterMission();
            Tool.GoToTalkScene();
            isFinished = true;
		} else if (isDone && isFinished) {
            Tool.FindMe().talkIndex = 4;
            Tool.FindMe().StartTalk();

        }
        return true;
	}

	private void afterMission()
	{
		hero = Tool.FindMyMissionPerson (Person.Story.MOTHER);
		Vector3 position = hero.transform.position;
		Vector3 scale = hero.transform.localScale;
		hero.transform.position = new Vector3 (position.x-1.2f, position.y + 1.2f, position.z);
	}
}

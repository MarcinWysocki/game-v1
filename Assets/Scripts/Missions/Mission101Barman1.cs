﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Missions;

public class Mission101Barman1 : IMission
{

    private const string STUFF = "TestStuff";
    public override void StartMission(string name)
    {
        if (!isStarted)
        {
            player = Tool.FindMe();
            isStarted = true;
            speach = "Hi, I am the barman here. What can I help you?";
            player.StartTalk();
            Debug.Log(speach);
        }
        else
        {
            CheckStatus();
        }
    }
    public override void CheckStatus()
    {

        //No idea yet
        if (false)
        {
            isDone = true;
            EndMission();
        }
        else
        {
            speach = "I have no mission now. In progress...";
            player.StartTalk();
            Debug.Log(speach);
        }
    }
    public override bool EndMission()
    {
        if (isDone && isFinished)
        {
            speach = "Good luck";
            Debug.Log(speach);
        }
        else if (isDone && !isFinished)
        {
            BattleAfter batttleAfter = player.GetComponent<BattleAfter>();
            batttleAfter.AddExperience(expForMission);
            speach = "Oow this is it. Thank you.";
            speach += Communicates.ReceiveCommunicate(expForMission, batttleAfter.lvlCommunicate);
            Debug.Log(speach);
            player.bag.removeStuff(new TestStuff());
            lastMove();
            isFinished = true;
        }
        player.StartTalk();
        return true;
    }
    private void lastMove()
    {
        hero = Tool.FindMyMissionPerson(Person.Story.ANON);
        Vector3 positionHero = hero.transform.position;
        hero.transform.position = new Vector3(positionHero.x - 4, positionHero.y, positionHero.z);
    }
}

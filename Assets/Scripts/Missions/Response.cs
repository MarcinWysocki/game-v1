﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Response {


    public string dialog;
    public int respect;
    public bool isfinish;
    public Response(string dialog, int respect, bool isfinish)
    {
        this.dialog = dialog;
        this.respect = respect;
        this.isfinish = isfinish;
    }
    public Response(int respect, bool isfinish)
    {
        this.respect = respect;
        this.isfinish = isfinish;
    }

}

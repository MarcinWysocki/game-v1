﻿
using UnityEngine;
using System.Collections;
using Assets.Scripts.Missions;

public class Mission007YoungMother : IMission {

	public override void StartMission(string name)
	{
        expForMission = 250;
        Tool.FindMe().currentMissionIndex = missionID;
        if (!isStarted) {
			isStarted = true;
            Tool.FindMe().talkIndex = 0;
            Tool.FindMe().StartTalk();
		} else {
			CheckStatus ();
		}
	}
	public override void CheckStatus()
	{
		IMission mission8 = Tool.GetStory().missions [8];
		if (mission8.isDone) {
			isDone = true;
            mission8.isFinished = true;

            EndMission ();
		} else {
            Tool.FindMe().talkIndex = 1;
            Tool.FindMe().StartTalk();
		}
	}
	public override bool EndMission()
	{
		if (isDone && isFinished) {
            Tool.FindMe().talkIndex = 3;
            Tool.FindMe().StartTalk();
        }
		else if (isDone && !isFinished) {
            BattleAfter batttleAfter = Tool.FindMe().GetComponent<BattleAfter>();
            batttleAfter.AddExperience (expForMission);
            speach = Communicates.ReceiveCommunicate(expForMission, batttleAfter.lvlCommunicate);
            isFinished = true;
            lastMove ();
            Tool.FindMe().talkIndex = 2;
            DialogManager.AddText(missionID, Tool.FindMe().talkIndex, 0, false, speach);
            Tool.GoToTalkScene();
        }
        return true;
	}

	private void lastMove(){
		hero = Tool.FindMyMissionPerson (Person.Story.RYDIA);
        GameObject girl = Tool.FindMyMissionPerson (Person.Story.LIDIA);
        girl.GetComponent<MissionID>().ID = 11;
        Vector3 positionHero = hero.transform.position;
        girl.transform.position = new Vector3(positionHero.x, positionHero.y-1, positionHero.z);
        

    }
}

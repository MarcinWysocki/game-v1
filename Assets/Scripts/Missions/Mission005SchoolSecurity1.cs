﻿using Assets.Scripts.Missions;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mission005SchoolSecurity1 : IMission {
    public override void StartMission(string Name)
    {
        expForMission = 25;
        Tool.FindMe().currentMissionIndex = missionID;
            if (!isStarted)
            {
                isStarted = true;
                Tool.FindMe().talkIndex = 0;
                Tool.FindMe().StartTalk();
                isDone = true;
            }
            else { CheckStatus(); }
    }
    public override void CheckStatus()
    {
        IMission mission = Tool.GetStory().missions[9];
        if (mission.isStarted)
        {
            isDone = true;
            EndMission();
        }
        else
        {
            Tool.FindMe().talkIndex = 1;
            Tool.FindMe().StartTalk();
        }
    }

    public override bool EndMission()
    {
        if (isDone && isFinished)
        {
            Tool.FindMe().talkIndex = 3;
            Tool.FindMe().StartTalk();
        }
        if (isDone && !isFinished)
        {
            BattleAfter batttleAfter = Tool.FindMe().GetComponent<BattleAfter>();
            batttleAfter.AddExperience(expForMission);
            Tool.FindMe().talkIndex = 2;
            DialogManager.AddText(missionID, Tool.FindMe().talkIndex, 0, false, Communicates.ReceiveCommunicate(expForMission, batttleAfter.lvlCommunicate));
            firstMove();
            isFinished = true;
            Tool.GoToTalkScene();
        }
        return true;
    }

    private void firstMove()
    {
        hero = Tool.FindMyMissionPerson(Person.Story.CHARWOMAN);
        Vector3 position = hero.transform.position;
        hero.transform.position = new Vector3(position.x - 2, position.y - 1, position.z);
        hero.GetComponent<MissionID>().ID = 12;
    }
}

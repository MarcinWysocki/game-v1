﻿using UnityEngine;
using Assets.Scripts.Missions;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Mission006SchoolSecurity2 : IMission
{

    public override void StartMission(string name)
    {
        Tool.FindMe().currentMissionIndex = missionID;
        if (!isStarted)
        {
            player = Tool.FindMe();
            isStarted = true;
            speach = "You are not ready";
            Debug.Log(speach);
            player.StartTalk();
        }
        else
        {
            CheckStatus();
        }
    }
    public override void CheckStatus()
    {
        if (allDefeated())
        {
            isDone = true;
            EndMission();
        }
        else
        {
            Tool.FindMe().talkIndex = 1;
            player.StartTalk();
        }
    }
    public override bool EndMission()
    {
        if (isDone && isFinished)
        {
            Tool.FindMe().talkIndex = 3;
        }
        if (isDone && !isFinished)
        {
            Tool.FindMe().talkIndex = 2;
            BattleAfter batttleAfter = player.GetComponent<BattleAfter>();
            batttleAfter.AddExperience(expForMission);
            lastMove();
            isFinished = true;
        }
        player.StartTalk();
        return true;
    }

    private bool allDefeated()
    {
        return Tool.allDefeated(4, getSceneNames());
    }

    private List<string> getSceneNames()
    {
        List<string> sceneNames = new List<string>();
        sceneNames.Add(SceneBase.CLASS1);
        sceneNames.Add(SceneBase.CLASS2);
        sceneNames.Add(SceneBase.CLASS3);
        return sceneNames;
    }

    private void lastMove()
    {
        hero = Tool.FindMyMissionPerson(Person.Story.SCHOOLSEC);
        Vector3 positionHero = hero.transform.position;
        hero.transform.position = new Vector3(positionHero.x, positionHero.y - 3, positionHero.z);
    }
}
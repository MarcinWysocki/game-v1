﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Missions;
using Assets.Scripts.DataBaste;

public class Mission012LostNecklace : IMission {

    
	public override void StartMission(string name)
	{
        prizeForMission = 100;
        expForMission = 100;
        Tool.FindMe().currentMissionIndex = missionID;
        if (!isStarted) {
            isStarted = true;
            Tool.FindMe().talkIndex = 0;
            Tool.FindMe().StartTalk();
        } else {
			CheckStatus ();
		}
	}
	public override void CheckStatus()
	{
		if (Tool.FindMe().bag.checkStuff(StoryItemList.SILVER_NECKLACE.GetName())) {
            isDone = true;
        } 
        EndMission();
    }
	public override bool EndMission()
	{
        if (isDone && isFinished)
        {
            Tool.FindMe().talkIndex = 3;
            Tool.FindMe().StartTalk();
            Debug.Log(speach);
        }
        else if (!isDone && !isFinished)
        {
            Tool.FindMe().talkIndex = 1;
            Tool.FindMe().StartTalk();
        }
        else if (isDone && !isFinished)
        {
            Tool.FindMe().talkIndex = 2;
            Tool.FindMe().StartTalk();
            BattleAfter batttleAfter = Tool.FindMe().GetComponent<BattleAfter>();
            batttleAfter.AddExperience(expForMission);
            Tool.FindMe().bag.removeStuff(StoryItemList.SILVER_NECKLACE);
            Tool.FindMe().bag.AddCash(prizeForMission);

            speach += Communicates.ReceiveCommunicate(prizeForMission, expForMission);
            Tool.FindMe().bag.removeStuff(new StoryItem());
            DialogManager.AddText(missionID, Tool.FindMe().talkIndex, 0, false, speach);
            isFinished = true;
        }
		return true;
	}
}

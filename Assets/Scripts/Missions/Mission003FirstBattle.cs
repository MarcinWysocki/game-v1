﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Missions;

public class Mission003FirstBattle : IMission {

	public override void StartMission(string name)
    {
        trainer = Tool.FindTrainer(Person.Trainer.ROBERT);
        Tool.FindMe().currentMissionIndex = missionID;
        expForMission = 10;
        if (!isStarted) {
            isStarted = true;
            Tool.FindMe().talkIndex = 0;
            firstMove ();
			isDone = true;
            Tool.FindMe().StartTalk();
        } else {
			CheckStatus ();
		}
	}
	public override void CheckStatus()
	{
        Debug.Log(trainer.name + " is defated: " + trainer.IsDefeated);
		if (trainer.IsDefeated) {
			isDone = true;
			EndMission ();
		} else {
            Tool.FindMe().talkIndex = 1;
            Tool.FindMe().StartTalk();
        }
	}
	public override bool EndMission()
	{
		if (isDone && isFinished) {
            Tool.FindMe().talkIndex = 3;
            Tool.FindMe().StartTalk();
        }
		if (isDone && !isFinished) {
            BattleAfter batttleAfter = Tool.FindMe().GetComponent<BattleAfter>();
            batttleAfter.AddExperience (expForMission);
            Tool.FindMe().talkIndex = 2;
            DialogManager.AddText(missionID,Tool.FindMe().talkIndex, 0, false, Communicates.ReceiveCommunicate(expForMission, batttleAfter.lvlCommunicate));           
			secondMove ();
            hero = Tool.FindMyMissionPerson(Person.Story.UNCELSAM);
            hero.GetComponent<MissionID>().ID = 4;
			isFinished = true;
            Tool.GoToTalkScene();
        }
        return true;
	}

	private void firstMove()
	{
		hero = Tool.FindMyMissionPerson (Person.Story.UNCELSAM);
		Vector3 position = hero.transform.position;
		hero.transform.position = new Vector3 (position.x - 5, position.y - 1, position.z);
	}
	private void secondMove(){
        hero = Tool.FindMyMissionPerson(Person.Story.UNCELSAM);
        Vector3 position = hero.transform.position;
		Vector3 scale = hero.transform.localScale;
		hero.transform.position = new Vector3 (position.x , position.y - 1, position.z);
	}
}

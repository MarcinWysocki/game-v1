﻿using UnityEngine;
using System.Collections;

public abstract class IMission {

	protected GameObject hero;
	protected Player player;
	protected Trainer trainer;
	protected string speach = "";
    public int missionID = 0;
    public int respect = 0;
	public bool isFinished = false;
	public bool isDone = false;
	public bool isStarted = false;
	public int expForMission = 0;
	public int prizeForMission = 0;
	public abstract void StartMission (string Name);
	public abstract void CheckStatus ();
	public abstract bool EndMission ();
    public const string UNREACHABLE = "This path should be impossible. Please check logs. Mission: ";
}

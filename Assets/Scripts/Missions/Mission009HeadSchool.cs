﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Missions;
using System.Collections.Generic;

public class Mission009HeadSchool : IMission {

    private const int reuiredLevel = 10;
	public override void StartMission(string name)
	{
        expForMission = 150;
        Tool.FindMe().currentMissionIndex = missionID;
        if (!isStarted) {
            isStarted = true;
            Tool.FindMe().talkIndex = 0;
            Tool.FindMe().StartTalk();
        } else {
			CheckStatus ();
		}
	}
	public override void CheckStatus()
	{
    	if (isHeadDefeated()){
			isDone = true;
			EndMission ();
		} else {
            Tool.FindMe().talkIndex = 1;
            Tool.FindMe().StartTalk();
        }

    }
	public override bool EndMission()
	{
		if (isDone && isFinished) {
            Tool.FindMe().talkIndex = 3;
            Tool.FindMe().StartTalk();
        }
		else if (isDone && !isFinished) {
            hero = Tool.FindMyMissionPerson(Person.Story.MAYOR_SECRETARY);
         //   hero.GetComponent<MissionID>().ID = 13;
            BattleAfter batttleAfter = Tool.FindMe().GetComponent<BattleAfter>();
            batttleAfter.AddExperience(expForMission);
            speach = Communicates.ReceiveCommunicate(expForMission, batttleAfter.lvlCommunicate);
            isFinished = true;
            lastmove();
            Tool.FindMe().talkIndex = 2;
            DialogManager.AddText(missionID, Tool.FindMe().talkIndex, 0, false, speach);
            Tool.GoToTalkScene();
        }
        return true;
	}

    private bool isHeadDefeated()
    {
        return Tool.allDefeated(1, getSceneNames());
    }

    private List<string> getSceneNames()
    {
        List<string> sceneNames = new List<string>();
        sceneNames.Add(SceneBase.HEADROOMSCHOOL);
        return sceneNames;
    }

    private void lastmove(){
		hero = Tool.FindMyMissionPerson (Person.Story.MAYOR_SECRETARY);
		Vector3 positionHero = hero.transform.position;
		hero.transform.position = new Vector3 (positionHero.x + 2, positionHero.y, positionHero.z);
	}

   //private bool isEnoughLevel(_Monster[] list)
   //{
   //    Debug.Log("List count:" + list.Length);
   //    foreach (_Monster m in list)
   //    {
   //        Debug.Log("Monster: " + m.name + " level: " + m.level);
   //        if (m.level >= reuiredLevel)
   //            return true;
   //    }
   //    return false;
   //}
}

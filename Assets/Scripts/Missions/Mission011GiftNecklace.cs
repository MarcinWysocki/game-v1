﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Missions;
using Assets.Scripts.DataBaste;

public class Mission011GiftNecklace : IMission {

	private Trainer[] trainersBro;

	public override void StartMission(string name)
	{
        Tool.FindMe().currentMissionIndex = missionID;
        if (!isStarted) {
			isStarted = true;
            Tool.FindMe().talkIndex = 0;
            Tool.FindMe().StartTalk();
			isDone = true;

        }
        CheckStatus();
    }
	public override void CheckStatus()
	{
			isDone = true;
			EndMission ();
	}
	public override bool EndMission()
	{
		if (isDone && isFinished) {
            Tool.FindMe().talkIndex = 1;
            Tool.FindMe().StartTalk();
        }
		else if (isDone && !isFinished) {
            Debug.Log("Items before: " + Tool.FindMe().bag.getItems().Count);
            Tool.FindMe().bag.addStuff(StoryItemList.SILVER_NECKLACE);
            BattleAfter batttleAfter = Tool.FindMe().GetComponent<BattleAfter>();
            speach = Communicates.ReceiveCommunicate(StoryItemList.SILVER_NECKLACE);

            DialogManager.AddText(missionID, Tool.FindMe().talkIndex, 0, false, speach);
            isFinished = true;
            Debug.Log("Items after: " + Tool.FindMe().bag.getItems().Count);
        }
       // player.StartTalk();
        return true;
	}


}

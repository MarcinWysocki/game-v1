﻿
using UnityEngine;
using System;
using System.Collections;

public class Mission000Introduction : IMission
{

    public override void StartMission(string name)
    {
        Tool.FindMe().currentMissionIndex = missionID;
        if (!isStarted)
        {
            isStarted = true;
            Tool.FindMe().talkIndex = 0;
            Tool.FindMe().StartTalk();
            isDone = true;
            Debug.Log("Mission started");
        }
        else { CheckStatus(); }
    }
    public override void CheckStatus()
    {
        Debug.Log("Check status...");
        EndMission(); 
    }
    public override bool EndMission()
    {
        if (isDone && isFinished)
        {
           Tool.FindMe().talkIndex = 4;
           Tool.FindMe().StartTalk();
        }
        if (isDone && !isFinished)
        {
            hero = Tool.FindMyMissionPerson(Person.Story.MOTHER);
            hero.GetComponent<MissionID>().ID = 2;
            isFinished = true;
            Tool.FindMe().talkIndex = 3;
            Tool.FindMe().StartTalk();
            MySceneManager scmn = Tool.FindMe().GetComponent<MySceneManager>();
        }



        return true;
    }
}

﻿using System;

namespace Assets.Scripts.Missions
{
    public class Communicates
    {
        private const string YOU = "\n[You received: ";
        public static String ReceiveCommunicate()
        {
            return YOU + "NOTHING";
        }
        public static String ReceiveCommunicate(int prizeForMission)
        {
            return YOU + prizeForMission + "$.]";
        }
        public static String ReceiveCommunicate(int prizeForMission, int exp)
        {
            return YOU + prizeForMission + "$ and " + exp + "exp.]";
        }
        public static String ReceiveCommunicate(int experience, String lvlCommunicate)
        {
            return YOU + experience + "exp. " + lvlCommunicate + "]";
        }
        public static String ReceiveCommunicate(int experience, String lvlCommunicate, Item newStaff)
        {
            return YOU + newStaff.GetName() + " and " + experience + "exp. " + lvlCommunicate + "]";
        }
        public static String ReceiveCommunicate(Item newStaff)
        {
            return YOU + newStaff.GetName() + "]";
        }
        public static String ReceiveCommunicate(_Monster monster)
        {
            return YOU + monster.name + " lvl " + monster.level + "]";
        }
    }
}

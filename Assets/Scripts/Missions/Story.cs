﻿using UnityEngine;
using System.Collections.Generic;

public class Story : MonoBehaviour {

	Player player;
	public IMission[] missions;
	void Start () {
		player = GetComponent<Player> ();
		missions = new IMission[100];
		generateMission ();
	}
	
	public void AssignPerson(string name, int IdPerson, int exp, int prize)
	{
		Debug.Log ("Name: " + name + ". ID=" + IdPerson + ". Mission: " + missions[IdPerson].ToString());
		missions [IdPerson].expForMission = exp;
		missions [IdPerson].prizeForMission = prize;
        missions[IdPerson].missionID = IdPerson;
        missions [IdPerson].StartMission (name);
	}

	private void generateMission()
	{
		missions [0] = new Mission000Introduction ();
		missions [1] = new Mission001FirstMonsterChoice ();
		missions [2] = new Mission002FirstMonsterDone ();
		missions [3] = new Mission003FirstBattle();
		missions [4] = new Mission004LastCheck ();
        missions [5] = new Mission005SchoolSecurity1();
        missions [6] = new Mission006SchoolSecurity2 ();
		missions [7] = new Mission007YoungMother();
		missions [8] = new Mission008GirlIsFound ();
		missions [9] = new Mission009HeadSchool ();
		missions [10] = new Mission010MayorsGirls ();
		missions [11] = new Mission011GiftNecklace ();
		missions [12] = new Mission012LostNecklace();
        missions [13] = new Mission013MayorsEnemy();
        missions [14] = new Mission014HeadOfBankSecretary();
    }
}

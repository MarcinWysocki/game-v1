﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Missions;

public class Mission010MayorsGirls : IMission {
	
	public override void StartMission(string name)
	{
        hero = Tool.FindMyMissionPerson(Person.Story.MAYOR);
        Tool.FindMe().currentMissionIndex = missionID;

        if (!isStarted) {
            isStarted = true;
            Tool.FindMe().talkIndex = 0;
            Tool.FindMe().StartTalk();
        } else {
			CheckStatus ();
        }
	}
	public override void CheckStatus()
	{
        IMission mission7 = Tool.FindMe().GetComponent<Story>().missions[7];
        if (mission7.isFinished)
        {
            isDone = true;
            EndMission();
        }
        else {
            Tool.FindMe().talkIndex = 2;
            Tool.FindMe().StartTalk();
        }

	}
	public override bool EndMission()
	{
		if (isDone && isFinished) {
            Tool.FindMe().talkIndex = 3;
            Tool.FindMe().StartTalk();
        }
		if (isDone && !isFinished) {

            hero.GetComponent<MissionID>().ID = 13;
            Vector3 position = hero.transform.position;

            hero.transform.position = position;
            Tool.FindMe().talkIndex = 1;
            Tool.FindMe().StartTalk();

            isFinished = true;
		}
        return true;
	}

}

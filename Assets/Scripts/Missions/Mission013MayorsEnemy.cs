﻿using Assets.Scripts.Missions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class Mission013MayorsEnemy : IMission
{
    public override void StartMission(string Name)
    {
        Tool.FindMe().currentMissionIndex = missionID;
        if (!isStarted)
        {
            isStarted = true;
            expForMission = 100;
            Tool.FindMe().talkIndex = 0;
            Tool.FindMe().StartTalk();
        }
        else
        {
            CheckStatus();
        }
    }

    public override void CheckStatus()
    {
        if (Tool.allDefeated(6, getSceneNames()))
        {
            isDone = true;
            EndMission();
        }
        else
        {
            Tool.FindMe().talkIndex = 1;
            Tool.FindMe().StartTalk();

        }
    }

    public override bool EndMission()
    {
        if (isDone && isFinished)
        {
            Tool.FindMe().talkIndex = 3;
            Tool.FindMe().StartTalk();
        }
        if (isDone && !isFinished)
        {
            Tool.FindMe().talkIndex = 2;
            Tool.FindMe().StartTalk();
            Item item = new RecommendationItem(Person.story[Person.Story.MAYOR]);
            Tool.FindMe().bag.addStuff(item);
            BattleAfter batttleAfter = Tool.FindMe().GetComponent<BattleAfter>();
            speach = Communicates.ReceiveCommunicate(item);
            DialogManager.AddText(missionID, Tool.FindMe().talkIndex, 0, false, speach);
            isFinished = true;
        }
        return true;
    }


    private List<string> getSceneNames()
    {
        List<string> sceneNames = new List<string>();
        sceneNames.Add(SceneBase.BANKUPSTAIRS);
        return sceneNames;
    }
}



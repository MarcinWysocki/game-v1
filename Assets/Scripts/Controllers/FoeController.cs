﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FoeController : MonoBehaviour {

    private _Monster myMonster;
    Battle battle;
    private MyPathFinding path;
    private Tile currentTile;
    private Tile nearest;
    List<Tile> playerTiles;
    private const string foeTag = "MonsterB";
    public static bool ready = true;
    public void CheckIfOpponentAttack()
    {
        if (isFoe(0)) { 
            Debug.Log("Foe Turn " + Tool.GetActiveController().name);
            battle = Tool.FindMe().GetComponent<Battle>();
            Debug.Log("QUEUE name: " + Tool.GetActiveController().name);
            battle.getBattleField().log.addToLog("Foe turn, " + Tool.GetActiveController().name + "\n");
            path = GetComponent<MyPathFinding>();
            currentTile = Tool.GetActiveController().GetComponentInParent<Tile>();
            playerTiles = getPlayerTiles();
            Debug.Log("Before move");
            moveFoeMonster();

        } else {
            Debug.Log("Player turn");
        }
    }


    private void moveFoeMonster()
    {
        nearest = findTheNearest();
        Debug.Log("The nearest adress: " + nearest.GetAdress());

        path.FindPath(currentTile, nearest, battle.getBattleField().battleFieldArea);

        List<Tile> myPath = path.GetPath();
        Tile goal = myPath[myPath.Count - 1];
        myPath.RemoveAt(myPath.Count - 1);
        myPath.RemoveAt(0);

        //Rusza się dopóki ma AP lub cel nie został osiągnięty
        foreach (Tile tile in myPath) {
            Debug.Log("Move foe to: " + tile.GetAdress());
            battle.getBattleField().GetBattleTactic().SaveCoordinates(currentTile, tile);
            currentTile.GetComponent<MouseClickOnTile>().setCurrentTile(currentTile);
            currentTile.GetComponent<MouseClickOnTile>().MoveMonster(tile);
            currentTile = tile;
            if (Tool.IsEmpty(Tool.GetCurrentMonster().GetCurrentAP())) {
                Debug.Log("Move end");
                break;
            }
        }
        //doFoeAttack(goal);
        Tool.ResetAP();
        battle.CheckRemainAP();
    }

    private void attackFoeMonster(Tile goal)
    {
        int counter = 1;
        // Attack until AP is enough and more than zero

        Debug.Log("Before loop");

       // while (isApEnough())
       // {
       if (isApEnough()) {
            Debug.Log("AttacK no. " + counter++);
            battle.getBattleField().SetPathLenght(battle.getBattleField().GetBattleTactic().CountPathLenght());
            battle.getBattleField().log.addToLog("Monster " + Tool.GetCurrentMonster().name 
                + Tool.GetCurrentMonster().level);
            DoFoeAttack(goal);
            //If monster is dead but AP are still over 0 find new monster
            if (IsPlayerDead(goal))
            {
                if (!Tool.IsEmpty(Tool.GetCurrentMonster().GetCurrentAP()))
                {
                    playerTiles.Remove(goal);
                    // Find new monster if anyone still live
                }
            }
        }
        if (playerTiles.Count > 0)
        {
            moveFoeMonster();
        }
        else
        {
            battle.CheckRemainAP();
        }
    }

    private bool isApEnough()
    {
        bool enough = !Tool.IsEmpty(Tool.GetCurrentMonster().GetCurrentAP()) 
            && Tool.GetCurrentMonster().attackList[0].GetAPrequired() <= Tool.GetCurrentMonster().GetCurrentAP();
        if (!enough)
        {
            Tool.ResetAP();
        }
        return enough;
    }
        
    private bool IsPlayerDead(Tile tile)
    {
        return null == tile.GetMonster();
    }
    private void DoFoeAttack(Tile goal)
    {
        Debug.Log("Current monster: " + Tool.GetCurrentMonster() + " Current AP: " + Tool.GetCurrentMonster().GetCurrentAP() + " Path lenght: " + battle.getBattleField().GetPathLength());
        if (isReadToAttack())
        {
            battle.SetAttack(Tool.GetCurrentMonster().attackList[0]);
            battle.setAttackSelected(true);
            goal.GetComponent<MouseClickOnTile>().DoAttack(Tool.GetActiveController(), goal.GetComponentInChildren<MonsterController>());
            battle.setAttackSelected(false);
        }
    }

	private Tile findTheNearest()
	{
		BattleFieldTactic tactic = Tool.FindBattleField ().GetBattleTactic ();
		Debug.Log ("Current: " + currentTile.GetAdress () + " first player: " + playerTiles [0].GetAdress());
		tactic.SaveCoordinates (currentTile, playerTiles [0]);
		int distance = tactic.CountPathLenght ();
		Tile nearest = playerTiles [0];
		foreach (Tile tile in playerTiles) {
			tactic.SaveCoordinates (currentTile, tile);
			int newDistance = tactic.CountPathLenght ();
			if (newDistance < distance) {
				distance = newDistance;
				nearest = tile;
			}
		}
		Debug.Log ("Nearest tile: " + nearest.GetAdress());
		return nearest;
	}
	private List<Tile> getPlayerTiles()
	{
		List<Tile> playerTiles = new List<Tile> ();
		foreach (var mon in QueuePriority.FirstQueue) {
			if (!isFoe (mon)) {
				playerTiles.Add (mon.GetComponentInParent<Tile>());
			}
		}
		Debug.Log ("Player tiles count: " + playerTiles.Count);
		return playerTiles;
	}

	private bool isFoe(int index)
	{
		return QueuePriority.priorityQueue [index].tag.Equals (foeTag);
	}
	private bool isFoe(MonsterController monster)
	{
		return monster.tag.Equals (foeTag);
	}
    private bool isReadToAttack()
    {
        return !Tool.IsEmpty(Tool.GetCurrentMonster().GetCurrentAP()) && battle.getBattleField().GetPathLength() == 1;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonManager : MonoBehaviour
{

    public const string POSITIVE = "ButtonPositive";
    public const string NEUTRAL = "ButtonNeutral";
    public const string NEGATIVE = "ButtonNegative";
    public const string BAR = "TalkBar";
    public const string NEXT = "ContinueButton";


    public static TalkDisplay getDisplay()
    {
        return GameObject.Find(BAR).GetComponent<TalkDisplay>();
    }

    public static DialogVersion getDialogButton(string name)
    {
        return GameObject.Find(name).GetComponent<DialogVersion>();
    }

    public static EndTalk getEndButton()
    { return GameObject.Find(NEXT).GetComponent<EndTalk>(); }

    public static DialogVersion[] getDialogArray()
    {
        DialogVersion[] array = new DialogVersion[3];
        array[0] = getDialogButton(POSITIVE);
        array[1] = getDialogButton(NEUTRAL);
        array[2] = getDialogButton(NEGATIVE);
        return array;
    }

    public static void hideDialogButtons()
    {
        foreach (var button in getDialogArray())
        {
            Destroy(button.gameObject);
            button.gameObject.SetActive(false);
        }
    }

    public static void hideDialogButton(string name)
    {
        getDialogButton(name).gameObject.SetActive(false);
    }



    public static void SetAvailabeOptions(int number)
    {
        for (int i = 3 - number; i > 0; i--)
        {
            getDialogArray()[i].gameObject.SetActive(false);
        }
    }

    public static void SetButtonsPosition()
    {
        GameObject canv = Tool.FindCanvas();
        Vector3 newSize;
        float canvWidth = canv.GetComponent<RectTransform>().rect.width;
        float canvheight = canv.GetComponent<RectTransform>().rect.height;
        //Talk bar
        newSize = new Vector2(canvWidth, canvheight / 4);
        setSize(getDisplay().gameObject, newSize);
        getDisplay().gameObject.transform.position = new Vector3(newSize.x / 2, newSize.y / 2, 0);

        //End bar
        newSize = new Vector2(canvWidth / 4, canvheight / 5);
        setSize(getEndButton().gameObject, newSize);
        getEndButton().gameObject.transform.position = new Vector3(newSize.x / 2, canvheight - newSize.y / 2);

        getEndButton().gameObject.SetActive(false);
        //Dialog array
        for (int i = 0; i < 3; i++)
        {
            newSize = new Vector2(canvWidth * 0.3f, canvheight / 5);
            setSize(getDialogArray()[i].gameObject, newSize);
            Vector3 newPos = new Vector3(canvWidth - newSize.x / 2, canvheight - newSize.y * (i + 0.5f));
            getDialogArray()[i].gameObject.transform.position = newPos;
        }
    }

    private static void setSize(GameObject go, Vector3 newSize)
    {
        go.GetComponent<RectTransform>().sizeDelta = newSize;
        go.GetComponentInChildren<Text>().GetComponent<RectTransform>().sizeDelta = newSize;
    }
}

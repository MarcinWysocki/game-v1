﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MySceneManager : MonoBehaviour
{

    private int allScenes;
    //  private bool saved = false;
    public List<Vector3>[] memoryOfPositionPerson;
    public List<Vector3>[] memoryOfPositionTrainer;
    public List<bool>[] memoryOfTrainerDefated;
    public List<int>[] personIDs;


  

    void Start()
    {
        allScenes = getCountAllReadyScenes();
        memoryOfPositionPerson = new List<Vector3>[allScenes];
        memoryOfPositionTrainer = new List<Vector3>[allScenes];
        memoryOfTrainerDefated = new List<bool>[allScenes];
        personIDs = new List<int>[allScenes];
        Debug.Log("Scenes available: " + allScenes);
        SceneBase.setDialogSceneIndex(getIndexBySceneName(SceneBase.DIALOG_SCENE_NAME));

    }

    void OnLevelWasLoaded(int level)
    {
        //Wczytuje parametry dla danej sceny jeśli kiedyś została juz odwiedzona
        //Debug.Log(memoryOfPositionPerson[level]);
        if (memoryOfPositionPerson[level] != null)
         loadPosition(level); 
    }
    public static int getCountAllReadyScenes()
    {
        return SceneManager.sceneCountInBuildSettings - 1;
    }
    public static int getIndexBySceneName(string name)
    {
        //Zwraca index sceny podjąc nazwę. Wykorzystuje do tego xpath.
       return SceneUtility.GetBuildIndexByScenePath("Assets/Scenes/" + name + ".unity");
    }

    public void CatchTrainersDetails()
    {
        Tool.FindMe().currentSceneIndex = getIndexScene();
        savePosition(getIndexScene());
        //Debug.Log("Saved position for scene: " + getIndexScene());
    }

    public static int getIndexScene()
    { return SceneManager.GetActiveScene().buildIndex; }
    public void CreatePositionList(List<Vector3> newList, bool isPerson, int indx)
    {

        if (isPerson)
        {
            //Debug.Log("Expected: " + indx + " MAX: " + memoryOfPositionPerson.Length);
            memoryOfPositionPerson[indx] = newList;
        }
        else
        {
            memoryOfPositionTrainer[indx] = newList;
        }
    }
    public void CreateList(List<bool> newList, int indx)
    {
        memoryOfTrainerDefated[indx] = newList;
    }
    public void CreateList(List<int> newList, int indx)
    {
        personIDs[indx] = newList;
    }

    private void savePosition(int indx)
    {
        //Debug.Log("Saving positon...");
        List<Vector3> positionsPerson = new List<Vector3>() { };
        List<Vector3> positionsTrainer = new List<Vector3>() { };
        List<bool> trainersDefated = new List<bool>() { };
        List<int> IDs = new List<int>() { };
        GameObject[] people = GameObject.FindGameObjectsWithTag("StoryPerson");
        foreach (GameObject person in people)
        {
            positionsPerson.Add(person.transform.position);
            IDs.Add(person.GetComponent<MissionID>().ID);
        }
        GameObject[] trainers = GameObject.FindGameObjectsWithTag("Trainer");
        foreach (GameObject person in trainers)
        {
            positionsTrainer.Add(person.transform.position);
            trainersDefated.Add(person.GetComponent<Trainer>().IsDefeated);
        }

        CreatePositionList(positionsPerson, true, indx);
        CreatePositionList(positionsTrainer, false, indx);
        CreateList(trainersDefated, indx);
        CreateList(IDs, indx);
        //    saved = true;
    }
    private void loadPosition(int indx)
    {
        GameObject[] people = GameObject.FindGameObjectsWithTag("StoryPerson");
        int counter = memoryOfPositionPerson[indx].Count;
        for (int i = 0; i < counter; i++)
        {
            MissionID mid = people[i].GetComponent<MissionID>();
            people[i].GetComponent<MissionID>().ID = personIDs[indx][i];
            people[i].transform.position = memoryOfPositionPerson[indx][i];
        }
        GameObject[] trainers = GameObject.FindGameObjectsWithTag("Trainer");
        for (int i = 0; i < trainers.Length; i++)
        {
            trainers[i].transform.position = memoryOfPositionTrainer[indx][i];
            trainers[i].GetComponent<Trainer>().IsDefeated = memoryOfTrainerDefated[indx][i];
        }
    }

}

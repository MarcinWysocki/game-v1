﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{

    public Vector2 moving = new Vector2(); 
    public bool canMove = true;
    public int stepCounter;
    private GameObject opponent;
    private Animator animator;
    private Player player;
    private Story story;

    private Conversation conversation;
    private Vector3 defaultScale;

    private bool meetingWithTrainer = false;
    private bool meetingWithDoctor = false;
    private bool meetingStory = false;

    List<Vector3> memoryOfPositionPerson;
    List<Vector3> memoryOfPositionTrainer;
    List<bool> memoryOfTrainerDefated;

    public PlayerMoveAbilities moveAbilities;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        player = GetComponent<Player>();
        story = GetComponent<Story>();
        stepCounter = 1;
        defaultScale = transform.localScale;

    }



    // Update is called once per frame
    void Update()
    {

        //Part of code resposible for moving, direction and animation
        moving = new Vector2(0, 0);
        if (canMove)
        {

            if (moveAbilities.isRight()) //
            {
                goRight();
            }
            else if (moveAbilities.isLeft())  //
            {
                goLeft();
            }
            else if (moveAbilities.isUp()) //
            {
                goUp();
            }
            else if (moveAbilities.isDown()) //
            {
                goDown();
            }

            //
         //  if (moving.x != 0 || moving.y != 0)
         //  {
         //      stepCounter++;
         //      if (0 == stepCounter % 250)
         //      {
         //          Debug.Log("Steps: " + stepCounter);
         //      }
         //  }

            if (new Vector2(0, 0) == moving)
            {
                animator.SetInteger("AnimState", 0);
            }

             
            //Start conversation with trainer or doctor
            if (moveAbilities.isAction()) // //
            {
                if (meetingWithTrainer)
                {

                    if (conversation == null)
                        Debug.Log("Conv is null");
                    try
                    {
                        if (conversation.battleAction)
                        {
                            Debug.Log("Battle action done..");
                            //Delegate with battle is initializing now
                            conversation.startBattleAction += GetComponent<Battle>().StartBattle;
                            //Next delegate initialization for this person is blocking
                            conversation.setFirstActionDone(ref conversation.battleAction);
                        }
                    }
                    catch (MissingReferenceException e)
                    {
                        Debug.Log("Conversation is null: " + conversation == null);
                    }
                    conversation.Talk();
                }
                if (meetingWithDoctor)
                {
                    if (conversation.healthAction)
                    {
                        Debug.Log("Health action done..");
                        //Delegate with health care is initializing now
                        conversation.startCureAction += GetComponent<HealthCare>().CheckCostAll;
                        //Next delegate initialization for this person is blocking
                        conversation.setFirstActionDone(ref conversation.healthAction);
                    }
                    conversation.startCureAction();   //ChooseKindOfPerson ();
                    Debug.Log("You met a doctor");
                }
                if (meetingStory)
                {

                    MissionID mId = getOpponent().GetComponent<MissionID>();
                    if (null != mId)
                    {
                         Tool.GetStory().AssignPerson(mId.name, mId.ID, mId.experience, mId.prize);
                    }
                    else
                    {
                        Debug.Log("mId is null!");

                    }
                }
            }
            //Threatment when you meet a doctor
            if (Input.GetKeyDown("h") && meetingWithDoctor)
            {
                Debug.Log("Cure cost: " + GetComponent<HealthCare>().GetCureCost());
                if (GetComponent<HealthCare>().GetCureCost() > 0)
                {
                    if (GetComponent<HealthCare>().CompareCostWithCash())
                    {
                        GetComponent<HealthCare>().CureAll();
                    }
                    else
                    {
                        Debug.Log("You have not enough money!");
                    }
                }
                else
                {
                    Debug.Log("You don't need healthcare now");
                }
            }
            if (Input.GetKeyDown("c"))
            {
                // Debug.Log("Current: " + SceneManager.GetActiveScene().name + " index: " + SceneManager.GetActiveScene().buildIndex + " Path: " + SceneManager.GetActiveScene().path + " INDEX2: " + SceneUtility.GetBuildIndexByScenePath(SceneManager.GetActiveScene().path));
            }
        }
    }





    void OnCollisionEnter2D(Collision2D target)
    {
        //Checking with what object is collision
        if (null != target)
        {
            switch (target.gameObject.tag)
            {
                case Person.TRAINER:
                    {
                        //Debug.Log("Meeting with trainer");
                        meetingWithTrainer = true;
                        Debug.Log("Collision with: " + target.gameObject.name);
                        opponent = target.gameObject;
                        conversation = opponent.GetComponent<Conversation>();
                    }
                    break;
                case Person.DOCTOR:
                    {
                        meetingWithDoctor = true;
                        Debug.Log("Collision with: " + target.gameObject.name);
                        opponent = target.gameObject;
                        conversation = opponent.GetComponent<Conversation>();
                    }
                    break;
                case Person.STORY:
                    {
                        //Debug.Log("Meeting story");
                        meetingStory = true;
                        //Debug.Log("Collision with: " + target.gameObject.name + " ID = " + target.gameObject.GetComponent<MissionID>().ID);
                        opponent = target.gameObject;
                        conversation = opponent.GetComponent<Conversation>();
                    }
                    break;
                default: //{Debug.Log ("This is annonymous person: " + target.gameObject.name);}
                    break;
            }
        }
    }

    void OnCollisionExit2D(Collision2D target)
    {
        if (null != target)
        {
            string mytag = target.gameObject.tag;
            if (Person.TRAINER.Equals(mytag) || Person.DOCTOR.Equals(mytag) || Person.STORY.Equals(mytag))
            {
                RestoreMeetings();
            }
        }
    }

    private void RestoreMeetings()
    {
        meetingWithTrainer = false;
        meetingWithDoctor = false;
        meetingStory = false;
        opponent = null;
     //   Debug.Log("meetingWithPlayer: " + meetingWithTrainer + ". MeetingWithDoctor: " + meetingWithDoctor + ". MeetingWithStory: " + meetingStory);
    }

    public Vector3 setPlayerOnTheLeftSite()
    {
        return new Vector3(defaultScale.x * (-1), defaultScale.y, defaultScale.z);
    }
    public Vector3 setPlayerOnTheRightSite()
    {
        return defaultScale;
    }
    public GameObject getOpponent()
    {
        return opponent;
    }

    private void goRight()
    {
        moving.x = 0.1f;
        animator.SetInteger("AnimState", 1);
        transform.localScale = setPlayerOnTheRightSite();
    }
    private void goLeft()
    {
        moving.x = -0.1f;
        animator.SetInteger("AnimState", 1);
        transform.localScale = setPlayerOnTheLeftSite();
    }
    private void goUp()
    {
        moving.y = 0.1f;
        animator.SetInteger("AnimState", 2);
    }
    private void goDown()
    {
        moving.y = -0.1f;
        animator.SetInteger("AnimState", 3);
    }
}

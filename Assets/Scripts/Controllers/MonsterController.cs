﻿using UnityEngine;
using System;
using System.Collections;

public class MonsterController : MonoBehaviour {

	public delegate void Myevent(int i);
	public event Myevent myevent;
	private _Monster myMonster;
	void Start () {
	}

	public MonsterController Clone()
	{
		return (MonsterController)this.MemberwiseClone ();
	}

	public void SetMonster(_Monster m)
	{
		myMonster = m;
	}
	public _Monster getMonster()
	{
		return myMonster;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionManager : MonoBehaviour
{

    private void OnLevelWasLoaded(int level)
    {
        //Debug.Log( MySceneManager.getIndexBySceneName("Scene00 - Conversation"));
        if (level == SceneBase.getDIALOG_SCENE_INDEX())
        {
            // Debug.Log("CHECK SPECIAL MISSIONS SCENE:" + Tool.FindMe().currentSceneIndex);
            checkSpecialMissions(Tool.FindMe().currentSceneIndex);
        }
    }

    private void checkSpecialMissions(int level)
    {
        switch (level)
        {
            case 2:
                if (Tool.GetStory().missions[0].isFinished && !Tool.GetStory().missions[1].isFinished)
                {
                    Debug.Log("Special mission on level: " + level);
                    firstMonsterChoice();
                }
                break;
            default:
                break;
        }
    }

    private void firstMonsterChoice()
    {
        FirstMonsterChoice fmc = new FirstMonsterChoice();
        foreach (var dial in ButtonManager.getDialogArray())
        {
            dial.GetComponent<Button>().onClick.RemoveAllListeners();
            dial.GetComponent<Button>().onClick.AddListener(delegate  { fmc.MonsterSelected(); });
        }
    }

    public static void ResetDelegates()
    {
        foreach (var dial in ButtonManager.getDialogArray())
        {
            dial.GetComponent<Button>().onClick.RemoveAllListeners();
            dial.GetComponent<Button>().onClick.AddListener(delegate { dial.SelectDialog(); });
        }
    }

}

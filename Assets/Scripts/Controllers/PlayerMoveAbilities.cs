﻿using UnityEngine;

public class PlayerMoveAbilities : MonoBehaviour
{

    private const bool isAndroid = false;
    int width = 800;
    int height = 500;
    int myconst = 30;
   

    public bool isRight()
    {
        if (isAndroid)
        {
            Debug.Log("Input count: " + Input.touchCount);
            if (Input.touchCount > 0)
            {
                Debug.Log(width + "x" + height);
                Touch t = Input.GetTouch(0);
                return t.position.x > width/2 + myconst && t.position.y > height / 2- myconst && t.position.y < height / 2 + myconst;
            }
            else return false;
        }
        else
        {
            return Input.GetKey("right") || Input.GetKey("d");
        }
    }

    public bool isLeft()
    {
        if (isAndroid)
        {
            Debug.Log("Input count: " + Input.touchCount);
            if (Input.touchCount > 0)
            {
                Touch t = Input.GetTouch(0);
                return t.position.x < width / 2 - myconst && t.position.y > height / 2 - myconst && t.position.y < height / 2 + myconst;
            }
            else return false;
        }
        else
        {
            return Input.GetKey("left") || Input.GetKey("a");
        }
    }

    public bool isUp()
    {
        if (isAndroid)
        {
            Debug.Log("Input count: " + Input.touchCount);
            if (Input.touchCount > 0)
            {
                Touch t = Input.GetTouch(0);
                return t.position.y > height / 2 + myconst && t.position.x > width / 2 - myconst && t.position.x < width / 2 + myconst;
            }
            else return false;
        }
        else
        {
            return Input.GetKey("up") || Input.GetKey("w");
        }
    }

    public bool isDown()
    {
        if (isAndroid)
        {
            Debug.Log("Input count: " + Input.touchCount);
            if (Input.touchCount > 0)
            {
                Touch t = Input.GetTouch(0);
                return t.position.y < height / 2 - myconst && t.position.x > width / 2 - myconst && t.position.x < width / 2 + myconst;
            }
            else return false;
        }
        else
        {
            return Input.GetKey("down") || Input.GetKey("s");
        }
    }

    public bool isAction()
    {
        if (isAndroid)
        {
            Debug.Log("Input count: " + Input.touchCount);
            if (Input.touchCount > 1)
            {
                Touch t = Input.GetTouch(1);
                return Input.touchCount > 1 && Input.GetTouch(1).position.x > 400 && Input.GetTouch(1).position.x < 600;
            }
            else return false;
        }
        else
        {
            return Input.GetKeyDown("e");
        }
    }


}
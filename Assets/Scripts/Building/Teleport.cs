﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Teleport : MonoBehaviour {

    public int sceneIndex = 0;
    public string sceneName;


   // public ScenesBase sceneN;

    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.gameObject.tag == "Player")
        {
            Object.DontDestroyOnLoad(Tool.FindMe().gameObject);
            Tool.FindMe().GetComponent<MySceneManager>().CatchTrainersDetails();
        //    Debug.Log("Scene name: " + sceneName + " & scene " + sceneN);
            if (!sceneName.Equals(""))
            {

                SceneManager.LoadScene(sceneName); }
            else
            {
                SceneManager.LoadScene(sceneIndex);
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BattleFieldButtons : MonoBehaviour
{
    int divde = 5;
    public BattleLog BuildBattleLog(BattleLog obj)
    {
        Vector3 point = new Vector3(0, 0, 0);
        GameObject canv = Tool.FindCanvas();
        float canvWidth = canv.GetComponent<RectTransform>().rect.width;
        float canvheight = canv.GetComponent<RectTransform>().rect.height;

        BattleLog battleLog = Canvas.Instantiate(obj, point, Quaternion.identity) as BattleLog;

        battleLog.name = "Log";
        SetText(battleLog, battleLog.name);
        battleLog.transform.SetParent(canv.transform, true);
        SetButtonSize(battleLog, canvWidth / divde, canvheight);

        float butWid = battleLog.GetComponent<RectTransform>().rect.width;
        float butHei = battleLog.GetComponent<RectTransform>().rect.height;
        battleLog.transform.position = new Vector3(butWid / 2, butHei / 2);
        battleLog.GetComponentInChildren<Text>().alignment = TextAnchor.UpperLeft;
        battleLog.GetComponentInChildren<Text>().color = Color.white;
        battleLog.resetText();
        battleLog.GetComponent<Button>().onClick.AddListener(delegate { battleLog.resetText(); });
    //    battleLog.GetComponent<Button>().interactable = false;
        battleLog.GetComponent<Button>().colors = SetLogColors(battleLog.GetComponent<Button>());

        return battleLog;
    }

    public MouseClickButtonAttack[] BuildAttackButtons(MouseClickButtonAttack obj)
    {
        Vector3 point = new Vector3(0, 0, 0);
        MouseClickButtonAttack[] attackButtons = new MouseClickButtonAttack[4];
        GameObject canv = Tool.FindCanvas();
        float canvWidth = canv.GetComponent<RectTransform>().rect.width;
        float canvheight = canv.GetComponent<RectTransform>().rect.height;
        for (int i = 0; i < attackButtons.Length; i++)
        {
            string myName = "Attack" + (i + 1);
            attackButtons[i] = Canvas.Instantiate(obj, point, Quaternion.identity) as MouseClickButtonAttack;
            attackButtons[i].name = myName;
            attackButtons[i].transform.SetParent(canv.transform, true);
            SetButtonSize(attackButtons[i], canvWidth / divde, canvheight / 8);

            float butWid = attackButtons[i].GetComponent<RectTransform>().rect.width;
            float butHei = attackButtons[i].GetComponent<RectTransform>().rect.height;
            attackButtons[i].transform.position = new Vector3(canvWidth - butWid / 2, canvheight - butHei * (3 / 2 * i + 1) + 10, 0);
            attackButtons[i].setIndexAttack(i);
            attackButtons[i].GetComponent<Button>().onClick.AddListener(attackButtons[i].SelectAttack);
        }
        AssignAttacksText(attackButtons);
        return attackButtons;
    }

    public MouseClickBattleDefence BuildBattleDefence(MouseClickBattleDefence obj)
    {
        Vector3 point = new Vector3(0, 0, 0);
        GameObject canv = Tool.FindCanvas();
        float canvWidth = canv.GetComponent<RectTransform>().rect.width;
        float canvheight = canv.GetComponent<RectTransform>().rect.height;

        MouseClickBattleDefence defenceButton = Canvas.Instantiate(obj, point, Quaternion.identity) as MouseClickBattleDefence;

        defenceButton.name = "Defence";
        SetText(defenceButton, defenceButton.name);
        defenceButton.transform.SetParent(canv.transform, true);
        SetButtonSize(defenceButton, canvWidth / divde, canvheight / 8);

        float butWid = defenceButton.GetComponent<RectTransform>().rect.width;
        float butHei = defenceButton.GetComponent<RectTransform>().rect.height;
        defenceButton.transform.position = new Vector3(canvWidth - butWid / 2, butHei / 2);
        defenceButton.GetComponent<Button>().onClick.AddListener(defenceButton.BackToDefence);
        defenceButton.GetComponent<Button>().colors = SetDefenceColors(defenceButton.GetComponent<Button>());
        return defenceButton;
    }

    private ColorBlock SetLogColors(Button button)
    {
        ColorBlock colors = button.colors; 
        colors.normalColor = Color.black;
        colors.highlightedColor = Color.black;
        colors.pressedColor = Color.black;
        return colors;
    }

    private ColorBlock SetDefenceColors(Button button)
    {
        ColorBlock colors = button.colors;
        colors.normalColor = Color.green;
        colors.highlightedColor = Color.green;
        colors.pressedColor = Color.blue;
        return colors;
    }

    public void AssignAttacksText(MouseClickButtonAttack[] attackButtons)
    {
        int attackCount = Tool.GetCurrentMonster().attackList.Count;
        bool active = true;
        string textOnButton;
        for (int i = 0; i < attackButtons.Length; i++)
        {
            if (i < attackCount)
            {
                textOnButton = Tool.GetCurrentMonster().attackList[i].GetName();
            }
            else
            {
                textOnButton = "Empty";
                active = false;
            }
            SetText(attackButtons[i], textOnButton);
            attackButtons[i].GetComponent<Button>().interactable = active;
        }
    }

    private void SetButtonSize(Component comp, float x, float y)
    {
        comp.GetComponent<RectTransform>().sizeDelta = new Vector2(x, y);
    }

    private void SetText(Component comp, string txt)
    {
        comp.GetComponentInChildren<Text>().text = txt;
    }
}

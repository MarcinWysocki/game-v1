﻿using UnityEngine;
using UnityEngine.UI;


public class BattleLog : MonoBehaviour
{
    public void addToLog(string txt)
    {
        gameObject.GetComponentInChildren<Text>().text += txt;
    }
    public void resetText()
    {
        gameObject.GetComponentInChildren<Text>().text = "LOG: \n";
    }
}

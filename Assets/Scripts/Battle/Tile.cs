﻿using UnityEngine;
using System.Collections.Generic;

public class Tile : MonoBehaviour {


	public List<Tile> adjecent;
	public Tile previous = null;
	public Tile nextTile = null;
	public string label = "";
	public int id = 0;

	private Vector2 adress;
	private MonsterController monsterOnTile;
	void Start () {
		adjecent = new List<Tile>();
	}

	public void ClearPrev(){
		previous = null;
	}

	public void SetAdress(int x, int y)
	{
		adress = new Vector2(x,y);
	}
	public Vector2 GetAdress()
	{
		return adress;
	}
	public void SetMonster(MonsterController m)
	{
		monsterOnTile = m;
	}
	public MonsterController GetMonster()
	{
		return monsterOnTile;
	}
	public void RemoveMonster()
	{
		monsterOnTile = null;
	}
}

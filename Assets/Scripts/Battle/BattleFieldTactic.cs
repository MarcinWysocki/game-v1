﻿using UnityEngine;
using System.Collections;

public class BattleFieldTactic : MonoBehaviour {

	Vector4 coordinates;
	public void SaveCoordinates(Tile current, Tile clicked)
	{
		coordinates = new Vector4 (current.GetAdress ().x, current.GetAdress ().y, clicked.GetAdress ().x, clicked.GetAdress ().y);
		Debug.Log ("Coordinates: " + coordinates);
	}
	public int CountPathLenght()
	{
		float distance = Mathf.Abs (coordinates.x - coordinates.z) + Mathf.Abs (coordinates.y - coordinates.w); 
		return (int)distance;
	}
}

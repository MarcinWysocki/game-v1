﻿using UnityEngine;
using System.Collections;

public class BattleAfter : MonoBehaviour {

	Player player;
	Trainer trainer;
	public string lvlCommunicate = " ";

	public void SetPeople(Player p, Trainer t)
	{
		player = p;
		trainer = t;
	}

	public void CheckEndBattle()
	{
        Debug.Log("Still live Player: " + QueuePriority.AliveCountA + " Still live Opponent: " + QueuePriority.AliveCountB);
		string text = "";
		if (Tool.IsEmpty(QueuePriority.AliveCountB)) {
			text = youWin();

		} else if (Tool.IsEmpty(QueuePriority.AliveCountA)) {
			text = youLost ();
		}
	}

    //Do dokończenia refaktor
    private string getWinnerText()
    {
        string text = "Congratulations! You won! You gained: " + sumExperience(trainer.pocket) + " exp. ";
        if (trainer.PayPrize() > 0)
        { text += "and " + trainer.PayPrize() + "$";
        }
        addBattleExperience(player.pocket, trainer.pocket);
        text += lvlCommunicate;
        return text;
    }

    private string youWin()
	{
        Tool.FindMe().talkIndex = 0;
        string text = "\n" + sumExperience(trainer.pocket) + " exp. ";
	    if (trainer.PayPrize () > 0)
	    { text += "and " + trainer.PayPrize () + "$";}
        
	    player.bag.AddCash(trainer.PayPrize());
	    addBattleExperience (player.pocket, trainer.pocket);
	    text += lvlCommunicate;
		gameObject.GetComponent<Battle>().EndBattle ();
        DialogManager.AddText(-1,Tool.FindMe().talkIndex, 0, false, text);
        trainer.IsDefeated = true;
        Vector3 oldPosition = trainer.transform.position;
        trainer.transform.position = TrainerDataBase.MoveAfterBattle(trainer.trainerID, oldPosition);
        Tool.GoToTalkScene();
        return text;
	}
	private string youLost()
	{ 
		string text = "";
        Tool.FindMe().talkIndex = 1;
        HealthCare health = GetComponent<HealthCare> ();
		health.CheckCostAll ();
     //   DialogManager.assignDialogs(Tool.FindMe().currentMissionIndex);
        if (health.CompareCostWithCash()) {
			text += "Your health care cost: " + health.GetCureCost();
			health.CureAll ();
			text += " Remaining money: " + player.bag.GetCash();
			gameObject.GetComponent<Battle>().EndBattle ();
        } else {
			text += " You have not enough money! You need: " + GetComponent<HealthCare> ().GetCureCost() + ". You have only: " + player.bag.GetCash() + " Sorry this is end.";
            //SceneManager.LoadScene ("END");
            Camera.main.orthographicSize = 3;
		}
        DialogManager.AddText(Tool.FindMe().currentMissionIndex,Tool.FindMe().talkIndex, 0, false, text);
        Tool.GoToTalkScene();
        return text;
	}

	private void addBattleExperience(Pocket winPocket, Pocket losePocket){
        //Zdobyte doświadczenie to suma HP przeciwnika podzielona przez liczbę pozostałych przy życiu stworów
		int newExperience = sumExperience(losePocket) / QueuePriority.AliveCountA;
		AddExperience (newExperience);
	}

	public void AddExperience(int newExp)
	{
        //Dodaje doświadczenie wszystkim zostałym przy życiu potworom
		lvlCommunicate = "";
		foreach (_Monster m in Tool.FindMe().pocket.getCurrentPocket()) {
            //Sprawca czy miejsce w pockecie jest zajęte oraz czy monster nadal żyje
			if (m!= null && !m.dead) {
				m.AddExperience (newExp);
                //Sprawdzenie czy monster osiągnął nowy poziom
				if (m.CheckLevel ()) {
					lvlCommunicate += m.name + " level up: " + m.level + " ";
				}
			}
		}
	}
    
    //Sumuje doświdaczenie z HP pokonanych wrogów
	private int sumExperience(Pocket losePocket)
	{
		int sum = 0;
		foreach (_Monster m in losePocket.getExistingPocket()) {
			if (m.dead) {
				sum += m.maxHP;
			}
		}
		Debug.Log ("Sum of experience: " + sum);
		return sum;
	}
}

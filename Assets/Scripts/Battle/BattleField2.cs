﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class BattleField2 : MonoBehaviour
{

    public Tile tile;
    public Tile[,] battleFieldArea = new Tile[9, 9];

    private BattleFieldButtons battleButtons;
    private BattleFieldMonsters monOnField;
    private BattleFieldTactic tactic;

    private Vector4 coordinates;

    private MouseClickButtonAttack attackTemplate;
    private MouseClickBattleDefence defenceTemplate;
    public Button temp;
    private MouseClickButtonAttack[] attackButtons;
    private MouseClickBattleDefence defenceButton;
    public BattleLog log;

    private MonsterController[] monstersA;
    private MonsterController[] monstersB;

    private Sprite[] sprites;
    public string resourceName;

    public int currentSprite = -1;

    int pathLenght;

    bool humanmove = false;
    public static bool IsDefenceButton = false;

    public void PrepareBattleField(Pocket playerPocket, Pocket foePocket)
    {
        initializeBattleComponents();
        buildBattleField(9);
        initializeMonsters(playerPocket, foePocket);
        initializeButtons();
        Tool.FindMe().GetComponent<PlayerController>().getOpponent().GetComponent<FoeController>().CheckIfOpponentAttack();
        Debug.Log("Battlefield position: " + gameObject.transform.position);
    }

    private void initializeBattleComponents()
    {
        tactic = gameObject.GetComponent<BattleFieldTactic>();
        monOnField = gameObject.GetComponent<BattleFieldMonsters>();
        battleButtons = gameObject.GetComponent<BattleFieldButtons>();
    }
    private void initializeButtons()
    {
        Vector3 point = Vector3.zero;
        Button temp2 = Instantiate(temp, point, Quaternion.identity) as Button;
        temp2.gameObject.AddComponent<MouseClickBattleDefence>();
        defenceButton = battleButtons.BuildBattleDefence(temp2.GetComponent<MouseClickBattleDefence>());
    
        Destroy(temp2.gameObject);
        IsDefenceButton = true;
        temp2 = Instantiate(temp, point, Quaternion.identity) as Button;

        temp2.gameObject.AddComponent<MouseClickButtonAttack>();
        attackButtons = battleButtons.BuildAttackButtons(temp2.GetComponent<MouseClickButtonAttack>());
        Destroy(temp2.gameObject);
        temp2 = Instantiate(temp, point, Quaternion.identity) as Button;

        temp2.gameObject.AddComponent<BattleLog>();
        log = battleButtons.BuildBattleLog(temp2.GetComponent<BattleLog>());
        Destroy(temp2.gameObject);
    }

    private void initializeMonsters(Pocket playePocket, Pocket foePocket)
    {
        monstersA = monOnField.FindMyMonsters(playePocket);
        monstersB = monOnField.FindFoeMonsters(foePocket);
        QueuePriority.AddToFirstQueue(monstersA, monstersB);
        QueuePriority.choosePriority();
    }

    public MouseClickButtonAttack[] GetAttackButtons()
    {
        return attackButtons;
    }
    public BattleFieldButtons GetButtonsOnBattle()
    {
        return battleButtons;
    }
    public BattleFieldMonsters GetMonstersOnField()
    {
        return monOnField;
    }
    public BattleFieldTactic GetBattleTactic()
    {
        return tactic;
    }
    public int GetPathLength()
    {
        return pathLenght;
    }
    public void SetPathLenght(int path)
    {
        pathLenght = path;
    }

    private void buildBattleField(int size)
    {
        Debug.Log("Build Battle field " + gameObject.name);
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                Tile clone = Instantiate(tile, transform.position, Quaternion.identity) as Tile;
                clone.name = (i * size + j).ToString();
                clone.id = Int32.Parse(clone.name);
                clone.tag = "Tile";
                battleFieldArea[i, j] = clone;
                battleFieldArea[i, j].SetAdress(i, j);
                battleFieldArea[i, j].transform.position = new Vector3(gameObject.transform.position.x + j + 3, gameObject.transform.position.y + i, gameObject.transform.position.z + 2);
            }
        }
    }


    // Use this for initialization
    Sprite mySprite()
    {
        resourceName = "Background";
        sprites = Resources.LoadAll<Sprite>(resourceName);

        if (currentSprite == -1)
        {
            int rand = UnityEngine.Random.Range(0, sprites.Length);
            Debug.Log("sprite: " + rand);
            currentSprite = rand;
        }
        else if (currentSprite > sprites.Length)
            currentSprite = sprites.Length - 1;

        return sprites[currentSprite];

    }

    public void resetBattleField()
    {
        foreach (MonsterController x in QueuePriority.FirstQueue)
        {
            Destroy(x.gameObject);
        }

        monstersA = null;
        monstersB = null;
        QueuePriority.priorityQueue = null;
        QueuePriority.TempQueue = null;

        foreach (Tile x in battleFieldArea)
        {
            Destroy(x.gameObject);
        }

        foreach (MouseClickButtonAttack x in attackButtons)
        {
            Destroy(x.gameObject);
        }
        Destroy(defenceButton.gameObject);
        Destroy(log.gameObject);
        IsDefenceButton = false;
    }
}

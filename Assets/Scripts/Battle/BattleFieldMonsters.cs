﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleFieldMonsters : MonoBehaviour {

	private List<_Monster> assignPlayerTempMonsters(Pocket playerPocket) {
		List<_Monster> tempPlayerMonsters = new List<_Monster> ();
		foreach (_Monster x in playerPocket.getExistingPocket()) {
			if (!x.dead)
				tempPlayerMonsters.Add (x);
		}
		Debug.Log ("Player monsters count: " + tempPlayerMonsters.Count);
		return tempPlayerMonsters;
	}

	private List<_Monster> assignFoeTempMonsters(Pocket opp) {
		List<_Monster> tempFoeMonster = new List<_Monster> ();
		foreach (_Monster x in opp.getExistingPocket()) {
				tempFoeMonster.Add (x);
		}
		Debug.Log ("Foe monsters count: " + tempFoeMonster.Count);
		return tempFoeMonster;
	}

	private MonsterController getMonsterView(string monsterName) {
		MonsterController myMonster = null;
		GameObject mon = Resources.Load<GameObject> ("MonsterArray");
		MonsterController[] monsterControllers = mon.GetComponentsInChildren<MonsterController> ();
		foreach (MonsterController item in monsterControllers) {
			Debug.Log("Monster name: " + monsterName + " Item name: " + item.name);
			if (monsterName.Equals (item.name)) {
				myMonster = item;
				break;
			}
		}
		if (myMonster == null) {
			Debug.Log ("Monster not assigned");
		}
		return myMonster;
	}
		
	public MonsterController[] FindMyMonsters(Pocket playerPocket) {
		List<_Monster> tempPlayerMonsters = assignPlayerTempMonsters(playerPocket);
		MonsterController[] monstersA = new MonsterController[tempPlayerMonsters.Count];
		for (int i = 0; i < tempPlayerMonsters.Count; i++) {
			Debug.Log (tempPlayerMonsters [i].name + " level: " + tempPlayerMonsters[i].level);
			MonsterController templateA = getMonsterView (tempPlayerMonsters [i].name);
			MonsterController clone = Instantiate (templateA, transform.position, Quaternion.identity) as MonsterController;
			clone.name = tempPlayerMonsters [i].name + "" + tempPlayerMonsters [i].level;
			clone.tag = "MonsterA";
			monstersA [i] = clone;
			monstersA [i].transform.position = new Vector3 (gameObject.transform.position.x + 2 * i + 3, gameObject.transform.position.y, gameObject.transform.position.z -1);
			monstersA [i].SetMonster (tempPlayerMonsters [i]);
			monstersA [i].transform.parent = Tool.FindBattleField().battleFieldArea [0, i *2].transform; //gameObject.transform;
			Tool.FindBattleField().battleFieldArea [0, i *2].SetMonster (monstersA [i]);
			Debug.Log (monstersA [i].name);
		}
		return monstersA;
	}

	public MonsterController[] FindFoeMonsters(Pocket foePocket) {
		List<_Monster> tempFoeMonster = assignFoeTempMonsters(foePocket);
		MonsterController[] monstersB = new MonsterController[tempFoeMonster.Count];
		for (int i = 0; i < tempFoeMonster.Count; i++) {
			Debug.Log (tempFoeMonster [i].name + " level: " + tempFoeMonster[i].level);
			MonsterController templateB = getMonsterView (tempFoeMonster [i].name);
			MonsterController clone = Instantiate (templateB, transform.position, Quaternion.identity) as MonsterController;
			clone.name = tempFoeMonster [i].name + "" + tempFoeMonster [i].level;
			clone.tag = "MonsterB";
			monstersB [i] = clone;
			monstersB [i].transform.position = new Vector3 (gameObject.transform.position.x + 2 * i + 3, gameObject.transform.position.y + 8, gameObject.transform.position.z -1);
			monstersB [i].SetMonster (tempFoeMonster [i]);
			monstersB [i].transform.parent = Tool.FindBattleField().battleFieldArea [8, i *2].transform;
			Tool.FindBattleField().battleFieldArea [8, i *2].SetMonster (monstersB [i]);
		}
		return monstersB;
	}
}

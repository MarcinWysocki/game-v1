﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Battle : MonoBehaviour {

    private Player player;
    public Trainer trainer;
	public BattleField2 battlefield;
	private BattleAfter battleAfter;
	public delegate void StillAlive(int i);
	public event StillAlive stillAlive;

	private bool attackSelected = false;
	private Attack selectedAttack;

	public void StartBattle()
	{
		battleAfter = gameObject.GetComponent<BattleAfter> ();
		PrepareBattle(gameObject, GetComponent<PlayerController>().getOpponent());
	}
		
	public void PrepareBattle(GameObject playerObject, GameObject opponentObject)
	{
		SetPlayers (playerObject, opponentObject);
		player.cameraConfig.SetCameraBeforeBattle (battlefield);
		trainer.DownloadMonsters (trainer.trainerID);
		battlefield.PrepareBattleField (player.pocket, trainer.pocket);
	}
        
	public bool isActionPointsEnough()
	{
        int currentAP = Tool.GetCurrentMonster().GetCurrentAP();
        int pathLength = battlefield.GetPathLength();
        return currentAP >= pathLength ? true : false;
	}

	public void CheckRemainAP()
	{
		if (!trainer.IsDefeated) {
			Debug.Log ("Action points: " + Tool.GetCurrentMonster().GetCurrentAP());
			if (Tool.IsEmpty(Tool.GetCurrentMonster().GetCurrentAP())) {
				QueuePriority.RemoveLastMonster ();
			
				if (!Tool.IsEmpty(QueuePriority.priorityQueue.Count)) {
					battlefield.GetButtonsOnBattle().AssignAttacksText (battlefield.GetAttackButtons());
					trainer.GetComponent<FoeController> ().CheckIfOpponentAttack ();
				}
			}
			if (QueuePriority.priorityQueue != null && Tool.IsEmpty(QueuePriority.priorityQueue.Count)) {
				QueuePriority.choosePriority ();
				battlefield.GetButtonsOnBattle().AssignAttacksText (battlefield.GetAttackButtons());
				trainer.GetComponent<FoeController> ().CheckIfOpponentAttack ();
			}
		}
	}

	public bool CompereTags(MonsterController A, MonsterController B)
	{
		//Zwraca TRUE jeśli tag komponentu i klikniętego obiektu nie są równe [przeciwnicy]
		return !A.tag.Equals (B.tag);
	}	
	public void DecreaseStillALive(MonsterController monB)
	{
		if (monB.tag.Equals("MonsterA")) {
			QueuePriority.AliveCountA --;
		}
		else if(monB.tag.Equals("MonsterB")) {
			QueuePriority.AliveCountB --;
		}

		battleAfter.CheckEndBattle ();
		Debug.Log ("Still alive: A=" + QueuePriority.AliveCountA + " B=" + QueuePriority.AliveCountB); 
	}
    public void SetPlayers(GameObject pl, GameObject opp)
    {
		player = pl.GetComponent<Player>();
		if (opp == null) {
			Debug.Log ("opp null: ");
		}
        player = pl.GetComponent<Player>();
        trainer = opp.GetComponent<Trainer>();
		battleAfter.SetPeople (player, trainer);
        Debug.Log("Battle started beetwen: " + player.name + " and " + trainer.name);
    }

    private void MonsterPresentation(Player pl, Trainer tr)
    {
        Tool.IterateMonsters(pl);
        Tool.IterateMonsters(tr);
    }

	public void EndBattle()
	{
		battlefield.resetBattleField ();
		//player.cameraConfig.RestoreCamera ();
	}

	public BattleField2 getBattleField()
	{
		return battlefield;
	}
	public void SetAttack(Attack at)
	{
		selectedAttack = at;
	}
	public Attack GetAttack()
	{
		return selectedAttack;
	}

	public bool isAttackSelected()
	{
		return attackSelected;
	}
	public void setAttackSelected(bool selected)
	{
		attackSelected = selected;
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Explode : MonoBehaviour {

	public BodyPart bodyPart;
	public int totalParts;
	public List<BodyPart> body;


	public void OnExplode(){
		body = new List<BodyPart> ();
		Destroy (gameObject);

		var t = transform;
		Debug.Log ("Explosion");
		for (int i = 0; i < totalParts; i++) {
			BodyPart clone = Instantiate(bodyPart, t.position, Quaternion.identity) as BodyPart;
			clone.GetComponent<Rigidbody2D>().AddForce(Vector3.right * (Random.Range (-50, 50)));
			clone.GetComponent<Rigidbody2D>().AddForce(Vector3.up * Random.Range(100, 400));
			body.Add (clone);
		}
	}
		
}

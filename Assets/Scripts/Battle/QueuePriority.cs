﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class QueuePriority{

	public static List<MonsterController> priorityQueue;
	public static List<MonsterController> TempQueue;
	public static List<MonsterController> FirstQueue;
	public static int AliveCountA, AliveCountB;

	public static void AddToFirstQueue(MonsterController[] monstersA, MonsterController[] monstersB)
	{
		AliveCountA = monstersA.Length; AliveCountB = monstersB.Length;
		priorityQueue = new List<MonsterController> ();
		FirstQueue = new List<MonsterController> ();
		for (int i = 0; i < monstersA.Length; i++) {
			FirstQueue.Add (monstersA[i]);
			monstersA [i].getMonster ().SetCurrentAP (monstersA [i].getMonster ().actionPoints);
		}
		for (int i = 0; i < monstersB.Length; i++) {
			FirstQueue.Add (monstersB[i]);
			monstersB [i].getMonster ().SetCurrentAP (monstersB [i].getMonster ().actionPoints);
		}
	}
	public static void choosePriority()
	{
		TempQueue = new List<MonsterController> ();
		for (int i = 0; i < FirstQueue.Count; i++) {
			FirstQueue [i].getMonster ().SetCurrentAP (FirstQueue [i].getMonster ().actionPoints);
			TempQueue.Add (FirstQueue[i].Clone());
		}

		while (TempQueue.Count > 0) {
			MonsterController theFastest = TempQueue [0];
			int j = 0;
			for (int i = 1; i < TempQueue.Count; i++) {
				if (TempQueue[i].getMonster().GetSpeed() >= theFastest.getMonster().GetSpeed()) {
					theFastest = TempQueue[i];
					j = i;
				}
			}
			TempQueue.RemoveAt (j);
			priorityQueue.Add (theFastest);
			theFastest.getMonster ().canMove = true;
			theFastest.getMonster ().moveDone = false;
		}
		string que = "Queue: ";
		foreach (var x in priorityQueue) {
			que += x.name + ", ";

		} 			
		Debug.Log (que);
	}

	public static void RemoveLastMonster()
	{
		Debug.Log ("Monster wil be removed: " + priorityQueue[0].name);
		priorityQueue.RemoveAt (0);
	}
	public static void RemoveEnemy(MonsterController m)
	{
		priorityQueue.Remove (m);
		FirstQueue.Remove (m);
	}

}

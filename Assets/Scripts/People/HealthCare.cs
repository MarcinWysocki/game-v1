﻿using UnityEngine;
using System.Collections;

public class HealthCare : MonoBehaviour {

	Player player;
	private int cureCost = 0;
	void Start () {
		player = GetComponent<Player> ();	
	}

	public bool CompareCostWithCash()
	{ 
		return player.bag.GetCash () >= cureCost;
	}

	public void CheckCostAll()
	{
		cureCost = 0;
		foreach (_Monster x in player.pocket.getExistingPocket()) {
			Debug.Log ("Current player pocket: " + player.pocket.getExistingPocket ().Length);
			if (x.currentHP < x.maxHP) {
				int cost = x.maxHP - x.currentHP;
				Debug.Log (x.name + " hurts and cost: " + cost);
				cureCost += cost;
			}
		}
		Debug.Log ("All cost: " + cureCost);
	}

	public void CureAll()
	{
		foreach (_Monster x in player.pocket.getCurrentPocket()) {
			if (x != null) {
				x.currentHP = x.maxHP;
				x.dead = false;
			}
		}
		player.bag.Pay (cureCost);
		cureCost = 0;
		Debug.Log ("Everyone is healthy");
	}

	public int GetCureCost()
	{
		Debug.Log ("GetCureCost: " + cureCost);
		return cureCost;
	}
}

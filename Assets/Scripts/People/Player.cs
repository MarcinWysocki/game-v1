﻿using UnityEngine;
using Assets.Scripts.UnitTests;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class Player : MonoBehaviour, ITrainer
{

    private PlayerController playerController;
    private Conversation conversation;
    public TalkButtons talkButtons;
    public int currentSceneIndex = 0;
    public int currentMissionIndex = -1;
    public Vector3 frozenPoz;

    private Vector2 moving;

    private GameObject opponent;
    public Pocket pocket;
    public Bag bag;
    public CameraConfig cameraConfig;
    public int talkIndex = 0;

    string speachText;
    public static Response[,] dialogTable;

    List<string> touches = new List<string>();
 
    void Start()
    {
 
        Debug.Log("Platform: " + Application.platform.ToString());

        InitializeComponents();
        GetComponent<Rigidbody2D>().gravityScale = 0;

        DontDestroyOnLoad(this);

        Debug.Log(SceneManager.GetActiveScene().name + " " + SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadScene(SceneBase.STARTMAP);
        Debug.Log(SceneManager.GetActiveScene().name + " " + SceneManager.GetActiveScene().buildIndex);
        StartGame();

        //  runTestsOnStart();
    }

    void OnLevelWasLoaded(int level)
    {
        Debug.Log("Loaded level: " + level + " All: " + SceneManager.sceneCountInBuildSettings);
        if (level == SceneBase.getDIALOG_SCENE_INDEX()) {
            Debug.Log("Talk scene was loaded");
            cameraConfig.SetConversationCamera();
            ButtonManager.SetButtonsPosition();
            DialogManager.UpdateTalkBar(talkIndex);
        }
    }

    void Update()
    {

        touches.Clear();
        for (int i = 0; i < Input.touchCount; i++)
        {
            Touch t = Input.GetTouch(i);
            string log = "Touch: " + i + " with position: " + t.position.ToString() + " and radius = " + t.radius;
            touches.Add(log);
        }
        moving = playerController.moving;
        Vector3 newPosition = new Vector3(transform.position.x + this.moving.x, transform.position.y + this.moving.y, 0);
        transform.position = newPosition;
    }

    private void OnGUI()
    {
        foreach (string s in touches)
        {
            GUILayout.Label(s);
        }
    }

    private void InitializeComponents()
    {
        playerController = GetComponent<PlayerController>();
        pocket = GetComponent<Pocket>();
        bag = GetComponent<Bag>();
        cameraConfig = GetComponent<CameraConfig>();
        talkButtons = GetComponent<TalkButtons>();
    }

    private void StartGame()
    {
        bag.AddCash(10);
        if (pocket != null)
        {
            //Pocket should be empty when is real start
          //  pocket.AddToCurrentPocket(new Phoenix(5));
           // Debug.Log (pocket.getCurrentPocket () [0].experience + "/" + pocket.getCurrentPocket () [0].experienceToNextLevel);
            //pocket.AddToCurrentPocket(new Angel(2));
            //Debug.Log (pocket.getCurrentPocket () [1].experience + "/" + pocket.getCurrentPocket () [0].experienceToNextLevel);
            //pocket.AddToCurrentPocket(new Angel(3));

            //pocket.AddToCurrentPocket(new Peasant(2));
            pocket.NotEmptyCount();
        }
    }

    public void StartTalk()
    {
        Debug.Log("Current mission index: " + currentMissionIndex);
        DialogManager.assignDialogs(currentMissionIndex);
        Tool.GoToTalkScene();
     //   ButtonManager.setAvailabeOptions(2);
    }

    public string getSpeachText()
    { return speachText;}

    public PlayerController GetPlayerController()
    {
        return playerController;
    }
    public Pocket GetPocket()
    {
        return pocket;
    }
    public string GetName()
    {
        return name;
    }
    private void runTestsOnStart()
    {
    //    PlayerNewGameTests tests = new PlayerNewGameTests(GetComponent<Player>());
    //    tests.PlayerInstanceIsReady();
    //    tests.PlayerControllerInstanceIsReady();
    //    tests.PlayerOpponentHasNoInstance();
    //    tests.PlayerPocketInstance();
    //    tests.PocketShouldBeEmpty();
    //    tests.NotEmptyCountMethod1();
    //    tests.NotEmptyCountMethod2();
    }

    public void ResetMissionIndex()
    {
        currentMissionIndex = -1;
    }
    public void ResetPosition()
    {
        transform.position = frozenPoz;
    }

}

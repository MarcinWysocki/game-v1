﻿using UnityEngine;
using System;


public class LookForward : MonoBehaviour {

	public Transform sightStart, sightEnd;

	public bool collision = false;

	private Trainer trainer;
	private Player player;
	private Conversation conversation;
	private Animator animator;
	public bool foundPlayer = false;
	private Vector2 stepValue;

    [Range(0.01f, 0.08f)]
    public float speed = 0.02f;


	// Use this for initialization
	void Start () {
		trainer = GetComponent<Trainer> ();
		player = Tool.FindMe ();
		conversation = trainer.GetComponent<Conversation> ();
		animator = GetComponent<Animator>();
		stepValue = trainer.direction * speed;
        animator.SetInteger("AnimState", 0);
    }
	
	// Update is called once per frame
	void Update () {

		if (!foundPlayer && !trainer.IsDefeated) {
				collision = Physics2D.Linecast (sightStart.position, sightEnd.position, 1 << LayerMask.NameToLayer ("Player"));
				Debug.DrawLine (sightStart.position, sightEnd.position, Color.green);
			
			if (collision) {
				//Need improve performance!!!
				GameObject opponentObject = player.GetPlayerController().getOpponent ();
				Tool.FreezePlayer (true);
				Vector3 pos = gameObject.transform.position;
				animator.SetInteger ("AnimState", 1);
				gameObject.transform.position = new Vector3 (pos.x + stepValue.x, pos.y + stepValue.y, pos.z);
				if (opponentObject != null) {
					animator.SetInteger ("AnimState", 0);
					foundPlayer = true;
					attackPlayer ();
				}
			}
		}
	}

	private void attackPlayer() {
		if (conversation.battleAction) {
			Debug.Log ("Battle action done..");
			//Delegate with battle is initializing now
			conversation.startBattleAction += player.GetComponent<Battle> ().StartBattle;
			//Next delegate initialization for this person is blocking
			conversation.setFirstActionDone (ref conversation.battleAction);
		}
		conversation.Talk ();
	}
}

﻿using UnityEngine;
using System.Collections.Generic;

public class Pocket : MonoBehaviour {

    private const int currentPocketSize = 5;
    private _Monster[] currentPocket = new _Monster[currentPocketSize];
    private List<_Monster> reservePocket;


	void Start () {
		if (gameObject.tag == "Player")
		{
			reservePocket = new List<_Monster>();
		}
	}

    public void AddToCurrentPocket(_Monster monster)
    {
        for (int i = 0; i < currentPocket.Length; i++)
        {
            if (currentPocket[i] == null)
            {
                currentPocket[i] = monster;
                break;
            }
			else if (currentPocket[currentPocket.Length-1] != null)
			{ 
				AddToReservePocket (monster);
				Debug.Log (monster.name + monster.level + "is added to reserved pocket");
				break;
			}
        }
    }

    private void AddToReservePocket(_Monster monster)
    {
		if (reservePocket == null) {
			reservePocket = new List<_Monster> ();
		}
		reservePocket.Add (monster);
		Debug.Log (monster.name + monster.level + " is added to reserved pocket. Count: " + reservePocket.Count);
    }

	public _Monster[] getCurrentPocket()
	{
		return currentPocket;
	}

    public _Monster[] getExistingPocket()
    {
        List<_Monster> list = new List<_Monster>();
        foreach (_Monster m in currentPocket)
        {
            if (m!=null) { list.Add(m); }
        }
        return list.ToArray();
    }

	public int NotEmptyCount()
	{
		int count = 0;
		foreach (_Monster x in currentPocket) {
			if (!(null == x)) {
				count++;
			}
		}
		Debug.Log ("Current pocket count: " + count);
		return count;
	}

    public void RemoveMonsterFromCpocket(int i)
    {
        if (i < currentPocketSize)
        { currentPocket[i] = null; }
        Debug.Log("Index out of range exception");

    }
}

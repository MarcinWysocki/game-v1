﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITrainer {

    string GetName();
    Pocket GetPocket();
}

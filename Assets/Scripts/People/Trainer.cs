﻿using UnityEngine;
using System.Collections.Generic;

public class Trainer : MonoBehaviour, ITrainer {

    public Pocket pocket;
    public int trainerID;
	private TrainerDataBase tdb;
	public int prize;
	public bool IsDefeated = false;
	public Vector2 direction;
    // Use this for initialization
    void Start () {
        pocket = GetComponent<Pocket>();
		tdb = new TrainerDataBase ();
		//downloadMonsters (trainerID);
    }

	public void DownloadMonsters(int id)
	{
		List<_Monster> monstersBase = tdb.getPocket (id);
		for (int i = 0; i < pocket.getCurrentPocket().Length; i++) {
			if (i < monstersBase.Count) {
				pocket.AddToCurrentPocket(monstersBase [i]);
				prize += monstersBase[i].maxHP; 
			}
			else {break;}
		}
	}
		
	public int PayPrize()
	{
		return prize;
	}

    public Pocket GetPocket()
    {
        return pocket;
    }
    public string GetName()
    {
        return name;
    }
}

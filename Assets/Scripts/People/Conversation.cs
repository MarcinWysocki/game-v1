﻿using UnityEngine;
using System.Collections;
using System;

public class Conversation : MonoBehaviour
{
	Trainer trainer;
    public delegate void StartAction();
    public StartAction startBattleAction;
	public StartAction startCureAction;

    private bool afterTalk = false;
    private bool goAway = false;
	private bool battlePermission = false;

	//Flags are needed to initialize delegates only one time during meeting
	public bool battleAction = true;
	public bool healthAction = true;





    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Talk()
	{
		trainer = GetComponent<Trainer> ();
		if (!trainer.IsDefeated) {
			trainer = GetComponent<Trainer> ();
			battlePermission = TrainerDataBase.GetBattlePermission (trainer.trainerID);
			if (battlePermission) {
				startBattleAction ();
				goAway = true;

			}
		} else if (trainer.IsDefeated) {
            Tool.FindMe().talkIndex = 0;
            DialogManager.AddText(-1,Tool.FindMe().talkIndex, 0, true, "Go away: ");
            Tool.GoToTalkScene();
			if (goAway) {		
				//gameObject.transform.localScale = new Vector3 (1, 1, 1);
				goAway = false;
			}
		}
	}
	public void setFirstActionDone(ref bool action)
	{
		action = false;
	}
}


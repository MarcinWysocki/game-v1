﻿using UnityEngine;
using System.Collections.Generic;

public class Bag : MonoBehaviour {

	private int cash;
	public List<Item> items;
	// Use this for initialization
	void Start () { items = new List<Item>();	}


	public void AddCash(int c)
	{
		cash += c;
		Debug.Log ("You have actually: " + cash + "$");
	}
	public void Pay(int c)
	{
		if (cash >= c) {
			cash -= c;
		} else { Debug.Log ("You have not enough money!");
		}
	}
	public int GetCash()
	{
		return cash;
	}
	public List<Item> getItems()
	{
		return items;
	}
	public void addStuff(Item s)
	{
		items.Add (s);
		Debug.Log ("Current bag count: " + items.Count);
	}
	public void removeStuff(Item s)
	{
		items.Remove (s);
		Debug.Log ("Current bag count: " + items.Count);
	}
	public bool checkStuff(string name)
	{
		bool match = false;
		foreach (Item s in items) {
			if (name.Equals (s.GetName ())) {
				match = true;
				Debug.Log ("I have the: " + s.GetName ());
				break;
			}
		}
		if (false == match) {
			Debug.Log ("You have no: " + name);
		}
		return match;
	}

}

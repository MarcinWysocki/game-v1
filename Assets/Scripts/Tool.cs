﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public static class Tool {



    //Player
    public static Player FindMe()
    {
        GameObject go = GameObject.FindGameObjectWithTag("Player");
        return go.GetComponent<Player>();
       
    }
    public static Story GetStory()
    {
        return FindMe().GetComponent<Story>();
    }
    public static IMission GetMission()
    {
        return Tool.GetStory().missions[Tool.FindMe().currentMissionIndex];
    }
    public static void AddExperience(int exp)
    {
        //Skraca dodawnie doświadczenie w innych klasach
        FindMe().GetComponent<BattleAfter>().AddExperience(exp);
    }
    public static void FreezePlayer(bool freeze)
    {
        PlayerController controller = FindMe().GetPlayerController();
        controller.GetComponent<Animator>().SetInteger("AnimState", 0);
        controller.canMove = !freeze;
    }

    //Scene
    public static void GoToTalkScene()
    {
        FindMe().currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
      //  Debug.Log("Go to Talk Scene");
        FindMe().GetComponent<MySceneManager>().CatchTrainersDetails();
        Debug.Log("Scene load: " + SceneBase.getDIALOG_SCENE_INDEX());
        SceneManager.LoadScene(SceneBase.getDIALOG_SCENE_INDEX());
    }


    //People
    private static GameObject FindMyMissionPerson(string name)
	{
		GameObject[] people = GameObject.FindGameObjectsWithTag ("StoryPerson");
		GameObject storyPerson = null;
		foreach (GameObject x in people) {
			if (name.Equals (x.name)) {
				storyPerson = x;
				break;
			}
		}
		return storyPerson;
	}
    public static GameObject FindMyMissionPerson(Person.Story person)
    {
        return FindMyMissionPerson(Person.story[person]);
    }

    private static Trainer FindTrainer(string name)
	{
		GameObject[] people = GameObject.FindGameObjectsWithTag ("Trainer");
		GameObject trainer = null;
		foreach (GameObject x in people) {
			if (name.Equals (x.name)) {
				trainer = x;
				break;
			}
		}
		return trainer.GetComponent<Trainer> ();
	}

    public static Trainer FindTrainer(Person.Trainer trainer)
    {
        return FindTrainer(Person.trainer[trainer]);
    }

    //Objects
	public static BattleField2 FindBattleField() {
		return FindMe ().GetComponent<Battle> ().battlefield;
	}
	public static GameObject FindCamera(){
		return GameObject.FindGameObjectWithTag ("MainCamera");
	}
    public static CameraFollow GetCameraFollow()
    {
        return FindCamera().GetComponent<CameraFollow>();
    }
    public static GameObject FindCanvas(){
		return GameObject.Find ("Canvas");
	}

    //Monsters
    public static MonsterController GetActiveController()
    {
        return QueuePriority.priorityQueue[0];
    }
    public static _Monster GetCurrentMonster()
    {
        return GetActiveController().getMonster();
    }
    public static void ResetAP()
    { Tool.GetCurrentMonster().ReduceCurrentAP(Tool.GetCurrentMonster().GetCurrentAP()); }
    public static void IterateMonsters(ITrainer tr)
    {
        Debug.Log("List of " + tr.GetName() + "'s monsters: ");
        foreach (_Monster mon in tr.GetPocket().getExistingPocket())
        {
                Debug.Log("Name: " + mon.name + ". Level: " + mon.level + ". Current HP: " + mon.currentHP + ".");
        }
    }

    //Is empty
    public static bool IsEmpty(List<MonsterController> list)
	{
		return list.Count == 0;
	}
	public static bool IsEmpty(int counter)
	{
		return counter == 0;
	}

    //Trainers status
    public static bool checkAreEveryPersonDefeated(List<bool> list)
    {
        //Sprawdza czy wszyscy zostali pokonani, przechodząc po liscie stanów
        foreach (bool isDefeated in list)
        {
            if (!isDefeated)
                return false;
        }
        return true;
    }
    public static bool checkAreEveryPersonDefeated(List<Trainer> list)
    {
        //Sprawdza czy wszyscy zostali pokonani, przechodząc po liscie trenerów
        foreach (Trainer trainer in list)
        {
            if (!trainer.IsDefeated)
                return false;
        }
        return true;
    }
    public static List<bool> PrepareTrainerStatusList(List<string> sceneNames)
    {
        //Tworzy listę stanów trenerów z wielu scen równocześnie
        List<bool> listDefeated = new List<bool>();
        var memory = Tool.FindMe().GetComponent<MySceneManager>().memoryOfTrainerDefated;
        int indx;
        //Pętla przechodzi przez poszczególne sceny i zbiera z nich stany
        foreach (string name in sceneNames)
        {
            //Spisanie indeksu danej sceny
            indx = MySceneManager.getIndexBySceneName(name);
            if (memory[indx] != null)
            {
                //Dodanie stanów z danej sceny do listy wszystkich potrzebnych stanów
                listDefeated.AddRange(memory[indx]);
            }
        }
        //Zwraca listę wszystkich potrzebnych stanów
        return listDefeated;
    }

    public static bool allDefeated(int count, List<string> sceneNames)
    {
        List<bool> listDefeated = PrepareTrainerStatusList(sceneNames);
        Debug.Log("List count: " + listDefeated.Count + ". Expexted: " + count);
        if (listDefeated.Count == count)
        {
            return checkAreEveryPersonDefeated(listDefeated);
        }
        else
        {
            return false;
        }
    }
}

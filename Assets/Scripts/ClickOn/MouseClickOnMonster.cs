﻿using UnityEngine;
using System.Collections;

public class MouseClickOnMonster : MonoBehaviour {

	//Script used when player click on monster
	void OnMouseDown()
	{
		MonsterController monA = Tool.GetActiveController();
		MonsterController monB = GetComponent<MonsterController> ();
		Battle battle = Tool.FindMe().GetComponent<Battle> ();
		MouseClickOnTile mc2;
		if (monA != null && monB != null) {
			mc2 = gameObject.GetComponentInParent<Tile> ().GetComponent<MouseClickOnTile> ();
			battle.getBattleField().GetBattleTactic().SaveCoordinates (Tool.GetActiveController().GetComponentInParent<Tile> (), gameObject.GetComponentInParent<Tile> ());
			battle.getBattleField().SetPathLenght(battle.getBattleField().GetBattleTactic().CountPathLenght ());
			mc2.TryAttack (monA,monB);
			battle.CheckRemainAP();
		}
	}
}

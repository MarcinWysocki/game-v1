﻿using UnityEngine;
using System.Collections;

public class Continue : MonoBehaviour {

	public Continue continueObject;
	private Continue continueButton;

    //public void DisplayContinue()
    //{
    //	GameObject canv = Tool.FindCanvas ();
    //	float canvWidth = canv.GetComponent<RectTransform> ().rect.width;
    //	float canvheight = canv.GetComponent<RectTransform> ().rect.height;
    //	Vector3 point = new Vector3 (0, 0, 0);
    //	continueButton = Canvas.Instantiate (continueObject, point, Quaternion.identity) as Continue;
    //	continueButton.GetComponent<RectTransform> ().sizeDelta = new Vector2 (canvWidth/4, canvheight/8);
    //	float width = continueButton.GetComponent<RectTransform> ().rect.width;
    //	float butHei = continueButton.GetComponent<RectTransform> ().rect.height;
    //	continueButton.transform.SetParent (canv.transform, true);
    //	continueButton.transform.position = new Vector3 (width/2, canvheight - butHei/2, 0);
    //
    //}

    public void ContinueGame()
	{
		Player player = Tool.FindMe ();
		player.talkButtons.talkBar.DestroyBar ();
		player.cameraConfig.RestoreCamera ();
		Debug.Log ("Continue game...");
		Destroy (gameObject);

	}

}

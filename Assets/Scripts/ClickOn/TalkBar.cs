﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TalkBar : MonoBehaviour {

	public void SetText(string text)
	{
		gameObject.GetComponentInChildren<Text> ().text = text;
	}
	public void DestroyBar()
	{
		Destroy (gameObject);
	}
}

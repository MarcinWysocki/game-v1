﻿using UnityEngine;
using System.Collections;

public class MouseClickButtonAttack : MonoBehaviour {

	Battle battle;
	private int indexAttack = -1;
	void Start () {
		battle = Tool.FindMe().GetComponent<Battle> ();
	}

	void OnMouseDown()
	{
		SelectAttack ();
	}

	public void SelectAttack()
	{
		Attack choosenAttack = chooseAttack ();
		battle.SetAttack (choosenAttack);

		if (battle.GetAttack () != null) {
			battle.setAttackSelected (true);
			Debug.Log ("Chosen attack: " + choosenAttack.GetName());
		}
	}
	private Attack chooseAttack()
	{
		Attack tempAt = null;
		int listCount = Tool.GetCurrentMonster ().attackList.Count;
		if (listCount > indexAttack) {
			tempAt = Tool.GetCurrentMonster ().attackList [indexAttack];
			Debug.Log ("Current monster: " + Tool.GetCurrentMonster() + " with attack: " + tempAt.GetName());
		} else {
			Debug.Log ("This attack is empty");
		}

		return tempAt;
	}

	public void setIndexAttack(int indx)
	{
		indexAttack = indx;
	}

}

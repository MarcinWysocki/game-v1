﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TalkButtons : MonoBehaviour
{

    public Button temp;
    public TalkBar talkBar;
    public Continue pressToContinue;
    void Start()
    {

    }

   void OnGUI()
   {
        
       if (Event.current.Equals(Event.KeyboardEvent("return")))
       {
           if (pressToContinue != null && talkBar != null)
           pressToContinue.ContinueGame();
       }


    }

    private void addListener()
    {
        pressToContinue.GetComponent<Button>().onClick.AddListener(delegate { pressToContinue.ContinueGame(); });

    }
    public void BuildContinue()
    {
        Vector3 point = new Vector3(0, 0, 0);
        Button temp2 = Instantiate(temp, point, Quaternion.identity) as Button;
        temp2.gameObject.AddComponent<Continue>();
        pressToContinue = DisplayContinue(temp2.GetComponent<Continue>());
        addListener();
        Destroy(temp2.gameObject);
    }

    public Continue DisplayContinue(Continue obj)
    {
        GameObject canv = Tool.FindCanvas();
        float canvWidth = canv.GetComponent<RectTransform>().rect.width;
        float canvheight = canv.GetComponent<RectTransform>().rect.height;
        Vector3 point = new Vector3(0, 0, 0);
        Continue cn = Canvas.Instantiate(obj, point, Quaternion.identity) as Continue;
        cn.GetComponent<RectTransform>().sizeDelta = new Vector2(canvWidth / 4, canvheight / 8);
        float width = cn.GetComponent<RectTransform>().rect.width;
        float butHei = cn.GetComponent<RectTransform>().rect.height;
        cn.transform.SetParent(canv.transform, true);
        cn.transform.position = new Vector3(width / 2, canvheight - butHei / 2, 0);
        cn.name = "Continue";
        cn.GetComponentInChildren<Text>().text = "Continue [Press Enter]";
        return cn;
    }

    public void BuildTalkBar()
    {
        Vector3 point = new Vector3(0, 0, 0);
        Button temp2 = Instantiate(temp, point, Quaternion.identity) as Button;
        temp2.gameObject.AddComponent<TalkBar>();
        talkBar = DisplayTalkBar(temp2.GetComponent<TalkBar>());
        Destroy(temp2.gameObject);
    }

    public TalkBar DisplayTalkBar(TalkBar obj)
    {

        GameObject canv = Tool.FindCanvas();
        float canvWidth = canv.GetComponent<RectTransform>().rect.width;
        float canvheight = canv.GetComponent<RectTransform>().rect.height;
        Vector3 point = new Vector3(0, 0, 0);
        TalkBar tl = Canvas.Instantiate(obj, point, Quaternion.identity) as TalkBar;
        tl.transform.SetParent(canv.transform, true);
        tl.GetComponent<RectTransform>().sizeDelta = new Vector2(canvWidth, canvheight / 4);
        float butwid = tl.GetComponent<RectTransform>().rect.width;
        float butHei = tl.GetComponent<RectTransform>().rect.height;

        tl.GetComponentInChildren<Text>().GetComponent<RectTransform>().sizeDelta = new Vector2(canvWidth - 100, canvheight / 4);

        tl.transform.position = new Vector3(canvWidth / 2, butHei / 2, 0);

        tl.tag = "TalkBar";
        tl.name = "TalkBar";
        obj = null;
        return tl;
    }

    public void RemoveButtons()
    {
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FirstMonsterChoice {

	public delegate void ChoiceDone();
	public ChoiceDone choiceDone;
	private IMission mission;
    private Event[] ev;
    private bool choiceIsReady = false;
    public const int choiceCount = 3;

    public FirstMonsterChoice()
    { }

    public void SelectMonster()
    {
        if (choiceIsReady)
        {
            Event e = Event.current;
            SetEvents();
            if (e.Equals(ev[0]) || e.Equals(ev[1]) || e.Equals(ev[2]))
            {
                Debug.Log(e);
                MonsterSelected();
                choiceIsReady = false;
            }
        }
    }


    public void ChoiceIsReady()
    { choiceIsReady = true; }

    private void SetEvents()
    {
        ev = new Event[choiceCount];
        for (int i = 0; i < choiceCount; i++)
        {
            ev[i] = Event.KeyboardEvent((i + 1).ToString());
        }
        
    }

    public void MonsterSelected(){
		Debug.Log ("Monster selected");
		Randomizer ();
		Player player = Tool.FindMe ();
		mission = player.GetComponent<Story> ().missions [1];
		mission.CheckStatus ();
        MissionManager.ResetDelegates();
	}

	private void Randomizer()
	{
		int random = Random.Range (1, 7);
		Player player = Tool.FindMe ();
		Debug.Log ("Random range: " + random);
		if (random <= 3) {
			player.pocket.AddToCurrentPocket (new Phoenix (4));
		} else if (4 == random || 5 == random) {
			player.pocket.AddToCurrentPocket (new Phoenix (5));
		} else if (6 == random) {
			player.pocket.AddToCurrentPocket (new Phoenix (6));
		}
        Debug.Log("You got: " + player.pocket.getCurrentPocket()[0].name);
    }
}

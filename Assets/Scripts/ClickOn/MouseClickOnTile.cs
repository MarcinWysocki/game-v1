﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MouseClickOnTile : MonoBehaviour {

	Tile currentTile;
	MyPathFinding pf;
	public List<BodyPart> body;
	public int i = 0;

	Battle battle;
	void Start () {
		body = new List<BodyPart> ();
		MonsterController mon = GetComponent<Tile>().GetComponentInChildren<MonsterController>();
	}

	// Update is called once per frame
	void OnMouseDown()
	{
		Player player = Tool.FindMe ();
		battle = player.GetComponent<Battle> ();
		setCurrentTile(Tool.GetActiveController().GetComponentInParent<Tile>());
		Tile clickedTile = gameObject.GetComponent<Tile> ();
		battle.getBattleField ().GetBattleTactic().SaveCoordinates (currentTile, clickedTile);

		MonsterController monsterOnClick = clickedTile.GetMonster ();

		if (monsterOnClick != null) {
			Debug.Log ("Clicked tile has monster: " + monsterOnClick.name);
			TryAttack (currentTile.GetMonster (), monsterOnClick);
		} else {
			pf = battle.getBattleField ().GetComponent<MyPathFinding> ();
			MoveMonster (clickedTile);
		}
		battle.setAttackSelected (false);
		battle.CheckRemainAP();
	}


	public void TryAttack(MonsterController monA, MonsterController monB)
	{
		if (battle == null) {
			battle = Tool.FindMe().GetComponent<Battle> ();
		}

		battle.getBattleField().SetPathLenght(battle.getBattleField().GetBattleTactic().CountPathLenght ());
		Debug.Log("Distance: " + battle.getBattleField().GetPathLength() + ". A tag: " + monA.tag + " .B tag: " + monB.tag + " Attack selected: " + battle.isAttackSelected());
		if (battle.isAttackSelected ()) {
			if (IsCloseEnough(monA,monB)) {
				DoAttack (monA, monB);
			}
		} else {
			Debug.Log ("Attack not selected");
		}
	}

	public void DoAttack(MonsterController monA, MonsterController monB)
	{
        //monA traktowany jest zawsze jako jednostka atakująca
		battle = Tool.FindMe().GetComponent<Battle> ();
		Tool.GetCurrentMonster ().ReduceCurrentAP (battle.GetAttack ().GetAPrequired());
        int damage = monA.getMonster().GetDamage(battle.GetAttack());
        string text = "Attack: " + battle.GetAttack().GetName() + " Attack AP: " + battle.GetAttack().GetAPrequired() + " Damage: " + damage + " \n";
        monB.getMonster ().ReduceCurrentHP (damage);
        Debug.Log(text);
        Tool.FindBattleField().log.addToLog(text);
        Tool.GetCurrentMonster ().canMove = false;
		Tool.GetCurrentMonster ().moveDone = true;
        //Log: Attacked monster name, current hp / max hp. Offense monster name, current AP
        text = monB.name + " HP: " + (monB.getMonster().currentHP >= 0 ? monB.getMonster().currentHP : 0) + "/" + monB.getMonster().maxHP + ". " + monA.name + " AP: " + Tool.GetActiveController().getMonster().GetCurrentAP() + "\n";
        Debug.Log (text);
        Tool.FindBattleField().log.addToLog(text);
        battle.setAttackSelected (false);
		if (monB.getMonster ().currentHP <= 0) {
			MonsterIsDead (monB);
		} 

	}
	private void MonsterIsDead(MonsterController monB)
	{
		Debug.Log (monB.name + " is dead!");
		monB.getMonster ().dead = true;
		QueuePriority.RemoveEnemy (monB);
		Explode explode = gameObject.GetComponentInChildren<Explode> ();
		//Destroy object on map
		Debug.Log("finish him");
		explode.OnExplode();
		//Remove from list still live
		battle.DecreaseStillALive (monB);
		//Remove script from clicked tile
		gameObject.GetComponent<Tile> ().SetMonster (null);
		Debug.Log ("Monster on tile: " + gameObject.GetComponent<Tile> ().GetMonster ());

	}
		
	private bool IsCloseEnough(MonsterController monA, MonsterController monB)
	{
		bool isEnough = false;
		if (battle.getBattleField ().GetPathLength () == 1) {
			if (battle.CompereTags (monA, monB)) {
				Debug.Log ("Current AP: " + Tool.GetCurrentMonster ().GetCurrentAP () + ". Required: " + battle.GetAttack ().GetAPrequired());
				if (Tool.GetCurrentMonster ().GetCurrentAP () >= battle.GetAttack ().GetAPrequired()) {
					isEnough = true;
				} else {
					Debug.Log ("You do not have enough required action points to attack");
				}
			} else {
				Debug.Log ("It's your friend");
				return false;
			}
		} else if (battle.getBattleField ().GetPathLength () > 1) {
			Debug.Log ("You are too far");
			return false;
		} else if (battle.getBattleField ().GetPathLength () == 0) {
			Debug.Log ("You are trying attack yourself!");
			return false;
		}
		return isEnough;
	}

	public void MoveMonster(Tile cT)
	{
		battle = Tool.FindMe().GetComponent<Battle> ();
		Tile clickedTile = cT;
		MonsterController monster = clickedTile.GetMonster ();

        //Counting path length from monster A to B
		battle.getBattleField().SetPathLenght(battle.getBattleField().GetBattleTactic().CountPathLenght ());
		if (monster != null) {
			Debug.Log ("Clicked tile has monster: " + monster.name);

		} else if (battle.isActionPointsEnough ()) {
			Debug.Log ("There is no monster");
			if (currentTile.GetMonster () == null)
				Debug.Log ("Current tile is null. Name: " + currentTile);

			Vector3 newPos = new Vector3 (clickedTile.transform.position.x, clickedTile.transform.position.y, currentTile.GetMonster ().transform.position.z);

			//Moving monster on new place
			currentTile.GetMonster ().transform.position = newPos;

			//New tile has old monster
			clickedTile.SetMonster (QueuePriority.priorityQueue [0]);

			//Monster has new parent
			QueuePriority.priorityQueue [0].transform.parent = clickedTile.transform;

			//Old tile has no monster
			currentTile.RemoveMonster();

			QueuePriority.priorityQueue [0].getMonster().ReduceCurrentAP (battle.getBattleField().GetPathLength());
		} 
		else {
			Debug.Log ("You have not enough action point for this move. Required: " + battle.getBattleField().GetPathLength() + ". Remain: " + QueuePriority.priorityQueue [0].getMonster ().GetCurrentAP());
		}
	}

	public void setCurrentTile(Tile t)
	{
		currentTile = t;
	}


}

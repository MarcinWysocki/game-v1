﻿
using UnityEngine;

public class SceneBase
{
    private static int DIALOG_SCENE_INDEX;

    public const string DIALOG_SCENE_NAME = "Scene00 - Conversation";
    public const string FAMILY_HOUSE = "Scene1 - FamillyHouse";
    public const string FAMILY_HOUSE_UP = "Scene1 - FamillyHouseUpstairs";
    public const string RETOPOLIS = "Scene2 - Retopolis";
    public const string CLASS1 = "Scene3 - Class1";
    public const string CLASS2 = "Scene3 - Class2";
    public const string CLASS3 = "Scene3 - Class3";
    public const string HEADROOMSCHOOL = "Scene3 - HeadRoom";
    public const string BANKHALL = "Scene5 - BankHall";
    public const string BANKUPSTAIRS = "Scene5 - BankUpstairs";
    public const string STARTMAP = FAMILY_HOUSE;

    public readonly int ScreanHeight = Screen.height;
    public readonly int ScreanWidth = Screen.width;

    public static void setDialogSceneIndex(int indx)
    {
        DIALOG_SCENE_INDEX = indx;
    }
    public static int getDIALOG_SCENE_INDEX()
    {
        return DIALOG_SCENE_INDEX;
    }
}

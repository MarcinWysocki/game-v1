﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person : MonoBehaviour
{
    public const string TRAINER = "Trainer";
    public const string DOCTOR = "Doctor";
    public const string STORY = "StoryPerson";
 
    public static Dictionary<Story, string> story = new Dictionary<Story, string>();
    public static Dictionary<Trainer, string> trainer = new Dictionary<Trainer, string>();

    public enum Story
    {
        MOTHER, FATHER, UNCELSAM, CHARWOMAN, SCHOOLSEC, RYDIA, LIDIA, MAYOR_SECRETARY, MAYOR, ANON, HEAD_BANK_SECRETARY
    }
    public enum Trainer
    {
        ROBERT, MARC,    ANON
    }

    void Start()
    {
        Debug.Log("Person start");
        setStory();
        setTrainer();
    }

    private static void setStory()
    {
        story.Add(Story.ANON, "");

        story.Add(Story.MOTHER, "Mother");
        story.Add(Story.FATHER, "Father");
        story.Add(Story.UNCELSAM, "Uncle Sam");
        story.Add(Story.CHARWOMAN, "Charwoman");
        story.Add(Story.SCHOOLSEC, "SchoolSecurity");
        story.Add(Story.RYDIA, "Rydia");
        story.Add(Story.LIDIA, "Lidia");
        story.Add(Story.MAYOR_SECRETARY, "MayorSecretary");
        story.Add(Story.MAYOR, "Mayor Leafouj");
        story.Add(Story.HEAD_BANK_SECRETARY, "Head bank secretary");
    }
    private static void setTrainer()
    {
        trainer.Add(Trainer.ANON, "");

        trainer.Add(Trainer.ROBERT, "Robert");
        trainer.Add(Trainer.MARC, "Marc");

    }
}

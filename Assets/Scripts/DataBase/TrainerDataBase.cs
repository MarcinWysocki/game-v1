﻿using UnityEngine;
using System.Collections.Generic;

public class TrainerDataBase
{
    List<_Monster> currentPocket;

    public TrainerDataBase()
    {
        currentPocket = new List<_Monster>();
    }

    public static bool GetBattlePermission(int id)
    {
        bool permission = false;
        switch (id)
        {
            case 1:
                //Robert
                int localid = Tool.FindMyMissionPerson(Person.Story.UNCELSAM).GetComponent<MissionID>().ID;
                permission = Tool.FindMe().GetComponent<Story>().missions[localid].isStarted;
                break;
            case 2:
                //Marc
                if (Tool.FindTrainer(Person.Trainer.ROBERT).GetComponent<Trainer>().IsDefeated)
                {
                    permission = true;
                }
                else
                {
                    Debug.Log("Before battle you need defeat Robert");
                }
                break;

            default:
                permission = true;
                Debug.Log("Wrong id for permission");
                break;
        }
        return permission;
    }
    public static Vector3 MoveAfterBattle(int id, Vector3 oldPos)
    {
        Vector3 position; ;
        switch (id)
        {
            case 9:
                position = new Vector3(oldPos.x, oldPos.y - 3, oldPos.z);
                break;
            default:
                position = oldPos;
                break;
        }
        return position;
    }

    public List<_Monster> getPocket(int id)
    {
        switch (id)
        {
            case 1:
                //Robert
                currentPocket.Add(new Angel(1));
                break;
            case 2:
                //Marc
                int lvl = Tool.FindMe().pocket.getCurrentPocket()[0].level - 1;
                currentPocket.Add(new Angel(lvl));
                break;
            case 3:
                //SchoolBoyA1
                currentPocket.Add(new Worm(1));
                break;
            case 4:
                //SchoolBoyA2
                currentPocket.Add(new Worm(1));
                currentPocket.Add(new Worm(3));
                break;
            case 5:
                //SchoolBoyA3
                currentPocket.Add(new Worm(5));
                break;
            case 6:
                //SchoolBoyA4
                currentPocket.Add(new Worm(1));
                currentPocket.Add(new Worm(1));
                currentPocket.Add(new Worm(5));
                //currentPocket.Add (new Worm (5));
                break;
            case 7:
                //Head of school

                currentPocket.Add(new Worm(4));
                currentPocket.Add(new Phoenix(1));
                currentPocket.Add(new Angel(4));
                //currentPocket.Add (new Worm (4));
                //currentPocket.Add (new Worm (4));
                break;
            case 8:
                //AlbertT 
                currentPocket.Add(new Worm(2));
                //currentPocket.Add (new Worm (3));
                //currentPocket.Add (new Worm (4));
                //currentPocket.Add (new Angel (2));
                break;
            case 9: //EcoGuy 
                currentPocket.Add(new Worm(2));
                currentPocket.Add(new Worm(3));

                currentPocket.Add(new Worm(4));
                currentPocket.Add(new Angel(2));
                break;
            case 10: //Herold 
                currentPocket.Add(new Worm(2));
                currentPocket.Add(new Worm(3));
                currentPocket.Add(new Worm(4));
                currentPocket.Add(new Angel(2));
                break;
            default:
                Debug.Log("Wrong id!");
                break;
        }
        return currentPocket;
    }
}

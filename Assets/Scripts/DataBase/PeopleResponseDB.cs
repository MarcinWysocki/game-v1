﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeopleResponseDB{

    private static void assignText(Response[,] old, int iteration, string[] minitable)
    {
        for (int i = 0; i < 4; i++)
        {
            old[iteration, i].dialog = minitable[i];
        }
    }



    private static void initializeDefaultResponseArray(Response[,] response, int talkCount)
    {
        for (int t = 0; t < talkCount; t++)
        {
            for (int i = 0; i < 4; i++)
            {
                response[t, i] = new Response(0, true);
            }
        }
    }

    private static void initializeDefaultResponseArray(Response[,] response)
    {
        initializeDefaultResponseArray(response, 4);
    }

    //Battle talk
    public static void ResponseBattle (Response[,] response)
    {
        initializeDefaultResponseArray(response, 2);
    }
    public static void MiniTableBattle(Response[,] response)
    {
        string[] minitable = new string[4];
        minitable[0] = "Congratulations! You won! You gained: ";
        minitable[1] = "Thanks for the battle.";
        minitable[2] = "Bye.";
        minitable[3] = "Ha! Fuck yeah!";
        assignText(response, 0, minitable);

        minitable[0] = "You lost! ";
        minitable[1] = "Eh! I have to train more";
        minitable[2] = "It's not fair";
        minitable[3] = "Shiiit...";
        assignText(response, 1, minitable);
    }

    //Mission 000 - Introduction
    public static void Response000(Response[,] response) {
        response[0, 0] = new Response(0, false);
        response[0, 1] = new Response(2, false);
        response[0, 2] = new Response(1, false);
        response[0, 3] = new Response(-1, false);

        response[1, 0] = new Response(0, true);
        response[1, 1] = new Response(1, true);
        response[1, 2] = new Response(1, true);
        response[1, 3] = new Response(1, true);

        response[2, 0] = new Response(0, true);
        response[2, 1] = new Response(0, true);
        response[2, 2] = new Response(0, true);
        response[2, 3] = new Response(0, true);

        response[3, 0] = new Response(0, true);
        response[3, 1] = new Response(0, true);
        response[3, 2] = new Response(0, true);
        response[3, 3] = new Response(0, true);

        response[4, 0] = new Response(0, true);
        response[4, 1] = new Response(0, true);
        response[4, 2] = new Response(0, true);
        response[4, 3] = new Response(0, true);
    }
    public static void MiniTable000(Response[,] response) {
        string[] minitable = new string[4];
        minitable[0] = "Welcome home my dear son. You've trained so long. I missed you but now... we have big problem. Please go fast talk to your father.";
        minitable[1] = "What's happend? Where I can find him?";
        minitable[2] = "Ok, where he is?";
        minitable[3] = "Eh, ok...";
        assignText(response, 0, minitable);

        minitable[0] = "Go upstairs. Go! Quickly!";
        minitable[1] = "Ok, I run";
        minitable[2] = "Ok";
        minitable[3] = "Eh, ok...";
        assignText(response, 1, minitable);

        minitable[0] = "There is no time. Go upstairs";
        minitable[1] = "Ok, I go";
        minitable[2] = "Ok";
        minitable[3] = "Eh, ok...";
        assignText(response, 2, minitable);

        minitable[0] = "Your first task is completed";
        minitable[1] = "Great!";
        minitable[2] = "Good";
        minitable[3] = "Ok";
        assignText(response, 3, minitable);

        minitable[0] = "Bye Bye";
        minitable[1] = "Bye";
        minitable[2] = "Bye";
        minitable[3] = "Bye";
        assignText(response, 4, minitable);
    }
    //Mission 001 - FirstMonsterChoice
    public static void Response001(Response[,] response)
    {
        response[0, 0] = new Response(0, false);
        response[0, 1] = new Response(1, false);
        response[0, 2] = new Response(1, false);
        response[0, 3] = new Response(1, false);

        response[1, 0] = new Response(0, true);
        response[1, 1] = new Response(0, true);
        response[1, 2] = new Response(0, true);
        response[1, 3] = new Response(0, true);

        response[2, 0] = new Response(0, true);
        response[2, 1] = new Response(1, true);
        response[2, 2] = new Response(1, true);
        response[2, 3] = new Response(1, true);

        response[3, 0] = new Response(0, true);
        response[3, 1] = new Response(0, true);
        response[3, 2] = new Response(0, true);
        response[3, 3] = new Response(0, true);

        response[4, 0] = new Response(0, true);
        response[4, 1] = new Response(0, true);
        response[4, 2] = new Response(0, true);
        response[4, 3] = new Response(0, true);
    }
    public static void MiniTable001(Response[,] response)
    {
        string[] minitable = new string[4];
        minitable[0] = "Hello my son. Your brother is lost. You have to find him. Please select your first friend..";
        minitable[1] = "Monster 1";
        minitable[2] = "Monster 2";
        minitable[3] = "Monster 3";
        assignText(response, 0, minitable);

        minitable[0] = "Congratulations! You have a new friend.";
        minitable[1] = "Yupi! I got it!";
        minitable[2] = "Good";
        minitable[3] = "OK";
        assignText(response, 1, minitable);

        minitable[0] = "You did not choose your comrade. Try again";
        minitable[1] = "OK";
        minitable[2] = "Ok";
        minitable[3] = "Eh, ok...";
        assignText(response, 2, minitable);

        minitable[0] = "You have just chosen your monster. Go and find your brother. I cannot help you anymore. Bye.";
        minitable[1] = "Thank you, bye";
        minitable[2] = "Bye";
        minitable[3] = "Meh..";
        assignText(response, 3, minitable);

        minitable[0] = "Please talk with your mom before";
        minitable[1] = "Thanks dad.";
        minitable[2] = "Ok";
        minitable[3] = "Oh, why?!";
        assignText(response, 4, minitable);
    }
    //Mission 002 - FirstMonsterDone
    public static void Response002(Response[,] response)
    {
        response[0, 0] = new Response(0, true);
        response[0, 1] = new Response(0, true);
        response[0, 2] = new Response(0, true);
        response[0, 3] = new Response(0, true);

        response[1, 0] = new Response(0, false);
        response[1, 1] = new Response(0, false);
        response[1, 2] = new Response(0, false);
        response[1, 3] = new Response(0, false);

        response[2, 0] = new Response(0, true);
        response[2, 1] = new Response(0, true);
        response[2, 2] = new Response(0, true);
        response[2, 3] = new Response(0, true);

        response[3, 0] = new Response(0, true);
        response[3, 1] = new Response(0, true);
        response[3, 2] = new Response(0, true);
        response[3, 3] = new Response(0, true);

        response[4, 0] = new Response(0, true);
        response[4, 1] = new Response(0, true);
        response[4, 2] = new Response(0, true);
        response[4, 3] = new Response(0, true);

        response[5, 0] = new Response(0, true);
        response[5, 1] = new Response(0, true);
        response[5, 2] = new Response(0, true);
        response[5, 3] = new Response(0, true);
    }
    public static void MiniTable002(Response[,] response)
    {
        string[] minitable = new string[4];
        minitable[0] = "Have you chosen your companion?";
        minitable[1] = "Yes, of course.";
        minitable[2] = "Yes";
        minitable[3] = "No.";
        assignText(response, 0, minitable);

        minitable[0] = "Hmm, let's see...";
        minitable[1] = "OK";
        minitable[2] = "Ok";
        minitable[3] = "Eh, ok...";
        assignText(response, 1, minitable);

        minitable[0] = "I see, you have a new friend. Great. It's for you.";
        minitable[1] = "Thank you.";
        minitable[2] = "Thanks";
        minitable[3] = "Bye...";
        assignText(response, 2, minitable);

        minitable[0] = "You do not have companies. Please back to your father";
        minitable[1] = "Yeah, I know";
        minitable[2] = "Nope";
        minitable[3] = "Yyy..";
        assignText(response, 3, minitable);

        minitable[0] = "I can not help you. Good luck!";
        minitable[1] = "Good bye";
        minitable[2] = "Bye";
        minitable[3] = "...";
        assignText(response, 4, minitable);

        minitable[0] = "Have you talked to your father?";
        minitable[1] = "Not yet";
        minitable[2] = "No";
        minitable[3] = "Yyy..";
        assignText(response, 5, minitable);
    }
    //Mission 003 - FirstBattle
    public static void Response003(Response[,] response)
    {
        initializeDefaultResponseArray(response);
    }
    public static void MiniTable003(Response[,] response)
    {
        string[] minitable = new string[4];
        minitable[0] = "Hello my kid. Do you remember uncle Sam? You are starting new adventure. To be sure that you are ready, go and fight with your cousin Robert";
        minitable[1] = "Ok, I will go";
        minitable[2] = "I don't remember you but ok..";
        minitable[3] = "I hate him. I will destroy him";
        assignText(response, 0, minitable);

        minitable[0] = "Did you beat Robert? Before you go, you have to do that.";
        minitable[1] = "Not yet Uncle Sam";
        minitable[2] = "No, I am going to go to him";
        minitable[3] = "No, but I will crush him";
        assignText(response, 1, minitable);

        minitable[0] = "You are almost ready..";
        minitable[1] = "Am I not ready yet?";
        minitable[2] = "But??";
        minitable[3] = "WTF?! I am ready!";
        assignText(response, 2, minitable);

        minitable[0] = "but..";
        minitable[1] = "But?";
        minitable[2] = "...";
        minitable[3] = "But?!!";
        assignText(response, 3, minitable);
    }
    //Mission 004 - Last Check
    public static void Response004(Response[,] response)
    {
        initializeDefaultResponseArray(response);
    }
    public static void MiniTable004(Response[,] response)
    {
        string[] minitable = new string[4];
        minitable[0] = "It is your last check. Go and fight with Rober's older brother Marc. Be careful. He is more dangerous than Robert.";
        minitable[1] = "I will be careful.";
        minitable[2] = "It should be easy..";
        minitable[3] = "The same dick as his brother.";
        assignText(response, 0, minitable);

        minitable[0] = "Have you talk with Marc? He is waiting for you";
        minitable[1] = "Not yet. I need to prepare myself. ";
        minitable[2] = "No, I am not ready.";
        minitable[3] = "Fuck this shit, I'm going now.";
        assignText(response, 1, minitable);

        minitable[0] = "Now you are mature trainer. You can start the adventure.";
        minitable[1] = "Great. I can not wait.";
        minitable[2] = "Good I am mature trainer.";
        minitable[3] = "Yeah, I am the best!";
        assignText(response, 2, minitable);

        minitable = leavetalk();
        assignText(response, 3, minitable);
    }
    //Mission 005 - School Security 1
    public static void Response005(Response[,] response)
    {
        initializeDefaultResponseArray(response);
    }
    public static void MiniTable005(Response[,] response)
    {
        string[] minitable = new string[4];
        minitable[0] = "Hi, I am Elma. I clean our school. We lost control on some classes.";
        minitable[1] = "Nice to meet you Elma.";
        minitable[2] = "It's standard in school.";
        minitable[3] = "Do you have problem with kids?!";
        assignText(response, 0, minitable);

        minitable[0] = "You should not go there";
        minitable[1] = "What's happend?";
        minitable[2] = "Why?";
        minitable[3] = "What are you talking about you old woman.";
        assignText(response, 1, minitable);

        minitable[0] = "You can go if you want.";
        minitable[1] = "Great. Thanks";
        minitable[2] = "That's good";
        minitable[3] = "You should let me faster";
        assignText(response, 2, minitable);

        minitable = leavetalk();
        assignText(response, 3, minitable);
    }

    //Mission 006 - School Security 2
    public static void Response006(Response[,] response)
    {
        initializeDefaultResponseArray(response);
    }
    public static void MiniTable006(Response[,] response)
    {
        string[] minitable = new string[4];
        minitable[0] = "Hey kid! Where are you going?";
        minitable[1] = "I would like to see head of school";
        minitable[2] = "Where is head?";
        minitable[3] = "It's not your bussiness";
        assignText(response, 0, minitable);

        minitable[0] = "You can not go there";
        minitable[1] = "I have to..";
        minitable[2] = "Why?";
        minitable[3] = "...";
        assignText(response, 1, minitable);

        minitable[0] = "Ok, go upstairs";
        minitable[1] = "Great. Thanks";
        minitable[2] = "Oki";
        minitable[3] = "...";
        assignText(response, 2, minitable);

        minitable = leavetalk();
        assignText(response, 3, minitable);
    }

    //Mission 007 - YoungMother
    public static void Response007(Response[,] response)
    {
        initializeDefaultResponseArray(response);
    }
    public static void MiniTable007(Response[,] response)
    {
        string[] minitable = new string[4];
        minitable[0] = "I'm Rydia. Did you see me daughter? Her name is Lidia. I hope she didn't drawn...";
        minitable[1] = "Sorry, but no. ";
        minitable[2] = "Nope.";
        minitable[3] = "You should better care of her";
        assignText(response, 0, minitable);

        minitable[0] = "Where is my little girl???";
        minitable[1] = "I don't know. I try to find her.";
        minitable[2] = "I don't know.";
        minitable[3] = "It waste of time.";
        assignText(response, 1, minitable);

        minitable[0] = "Thanks God and thank you. You really found her. I do not how to repay you.";
        minitable[1] = "Oh. It was my duty";
        minitable[2] = "Oh no problem";
        minitable[3] = "Next time be careful.";
        assignText(response, 2, minitable);

        minitable = leavetalk();
        assignText(response, 3, minitable);
    }
    //Mission 008 Girl is found 
    public static void Response008(Response[,] response)
    {
        initializeDefaultResponseArray(response);
    }
    public static void MiniTable008(Response[,] response)
    {
        string[] minitable = new string[4];
        minitable[0] = "... Mommy? ...";
        minitable[1] = "Sorry, but no";
        minitable[2] = "Nope.";
        minitable[3] = "Fucking kid.";
        assignText(response, 0, minitable);

        minitable[0] = "... Mommy? ...";
        minitable[1] = "I know where is your mom.";
        minitable[2] = "I will help you.";
        minitable[3] = "I will back with your mom. Don't move.";
        assignText(response, 1, minitable);

        minitable[0] = "I will wait";
        minitable[1] = "Great. Thanks";
        minitable[2] = "Ok";
        minitable[3] = "Shut up.";
        assignText(response, 2, minitable);

        minitable = leavetalk();
        assignText(response, 3, minitable);
    }
    //Mission 009 Head of school
    public static void Response009(Response[,] response)
    {
        initializeDefaultResponseArray(response);
    }
    public static void MiniTable009(Response[,] response)
    {
        string[] minitable = new string[4];
        minitable[0] = "Boss has problem with head of school. She barricaded herself in school office.";
        minitable[1] = "I will help him";
        minitable[2] = "Ok.";
        minitable[3] = "Oh shit";
        assignText(response, 0, minitable);

        minitable[0] = "Sorry, boss is busy right now.";
        minitable[1] = "Oh no";
        minitable[2] = "When can I talk with him?";
        minitable[3] = "I have to talk now...";
        assignText(response, 1, minitable);

        minitable[0] = "Please, come in";
        minitable[1] = "Thank you";
        minitable[2] = "...";
        minitable[3] = "Finally..";
        assignText(response, 2, minitable);

        minitable = leavetalk();
        assignText(response, 3, minitable);
    }

    //Mission 010 Mayor girla
    public static void Response010(Response[,] response)
    {
        initializeDefaultResponseArray(response);
    }
    public static void MiniTable010(Response[,] response)
    {
        string[] minitable = new string[4];
        minitable[0] = "I don't have time. I am worry about my girls, Lidia and Rydia..";
        minitable[1] = "Everything will be ok";
        minitable[2] = "I will find them";
        minitable[3] = "Who cares...";
        assignText(response, 0, minitable);

        minitable[0] = "Did you find her? Thanks God..";
        minitable[1] = "Yes, Lidia and Rydia are toghether near to lake";
        minitable[2] = "Yes, I did";
        minitable[3] = "I found this stupid kid";
        assignText(response, 1, minitable);

        minitable[0] = "My daughter is lost. I have no time.";
        minitable[1] = "I'm sorry";
        minitable[2] = "I will back later";
        minitable[3] = "It's your problem.";
        assignText(response, 2, minitable);

        minitable = leavetalk();
        assignText(response, 3, minitable);
    }

    //Mission 011 Gift Necklace
    public static void Response011(Response[,] response)
    {
        initializeDefaultResponseArray(response, 2)
;    }
    public static void MiniTable011(Response[,] response)
    {
        string[] minitable = new string[4];
        minitable[0] = "Thank you for help. I can give this silver necklace. I've found it in the forrest.";
        minitable[1] = "Thank you very much.";
        minitable[2] = "Thanks.";
        minitable[3] = "Give it to me..";
        assignText(response, 0, minitable);

        minitable = leavetalk();
        assignText(response, 1, minitable);
    }

    //Mission 012 Lost necklace
    public static void Response012(Response[,] response)
    {
        initializeDefaultResponseArray(response);
    }
    public static void MiniTable012(Response[,] response)
    {
        string[] minitable = new string[4];
        minitable[0] = "Have you seen my necklace? I've lost in forest. Could you help me to find?";
        minitable[1] = "Yes, sure.";
        minitable[2] = "Ok, I will find it";
        minitable[3] = "fuuuuu..";
        assignText(response, 0, minitable);

        minitable[0] = "Do you have it?";
        minitable[1] = "Not yet, but I am working on it";
        minitable[2] = "No, I don't";
        minitable[3] = "I do not have your fucking silver whatever";
        assignText(response, 1, minitable);

        minitable[0] = "Oh you found it. Thank you. It's for you";
        minitable[1] = "Thank you";
        minitable[2] = "Thanks.";
        minitable[3] = "It could be more";
        assignText(response, 2, minitable);

        minitable = leavetalk();
        assignText(response, 3, minitable);
    }

    //Mission 013 Mayor Enemy
    public static void Response013(Response[,] response)
    {
        initializeDefaultResponseArray(response);
    }

    public static void MiniTable013(Response[,] response)
    {
        string[] minitable = new string[4];
        minitable[0] = "I have last request for you. Herold from bank is my enemy. Please beat him.";
        minitable[1] = "Yes, no problem.";
        minitable[2] = "Ok, I start";
        minitable[3] = "I will destroy him!";
        assignText(response, 0, minitable);

        minitable[0] = "Why he is still live?";
        minitable[1] = "I had no time. I'm going now";
        minitable[2] = "I don't know..";
        minitable[3] = "Because yes..";
        assignText(response, 1, minitable);

        minitable[0] = "Great. You won. You have my recommendation";
        minitable[1] = "Thank you";
        minitable[2] = "Thanks.";
        minitable[3] = "Where is the money?!";
        assignText(response, 2, minitable);

        minitable = leavetalk();
        assignText(response, 3, minitable);
    }

    //Mission 014 Head bank secretary
    public static void Response014(Response[,] response)
    {
        initializeDefaultResponseArray(response, 3);
    }

    public static void MiniTable014(Response[,] response)
    {
        string[] minitable = new string[4];
        minitable[0] = "My boss is busy. Please come back later";
        minitable[1] = "Ok.";
        minitable[2] = "But..";
        minitable[3] = "No!";
        assignText(response, 0, minitable);

        minitable[0] = "You can go";
        minitable[1] = "Great!";
        minitable[2] = "Ok...";
        minitable[3] = "I know.";
        assignText(response, 1, minitable);


        minitable = leavetalk();
        assignText(response, 2, minitable);
    }

    private static string[] leavetalk()
    {
        string[] leave = new string[4];
        leave[0] = bye();
        leave[1] = "See you";
        leave[2] = "Bye";
        leave[3] = "...";
        return leave;
    }

    private static string bye()
    {
        List<string> list = new List<string> {"Good luck!", "Bye!", "See you later.", "Have a nice day." };
        return list[Random.Range(0, list.Count)];
    }
}

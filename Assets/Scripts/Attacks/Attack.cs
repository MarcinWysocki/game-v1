﻿using UnityEngine;
using System.Collections;

public abstract class Attack {

	protected string name;
    protected int power;
    protected int apRequired;

    public string GetName()
    { return name; }
    public int GetPower()
    { return power; }
    public int GetAPrequired()
    { return apRequired; }
}

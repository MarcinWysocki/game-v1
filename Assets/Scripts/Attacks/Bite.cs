﻿public class Bite : Attack
{
        public Bite()
        {
            this.name = "Bite";
            this.power = 5;
            this.apRequired = 2;
        }
}
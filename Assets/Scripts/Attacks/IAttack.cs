﻿using System.Collections.Generic;

public interface IAttack
{
    List<Attack> SetFutureAttack();
    List<int> SetPromotionLVL();
}
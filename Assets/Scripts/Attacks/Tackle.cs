﻿class Tackle : Attack
{
    public Tackle()
    {
        this.name = "Tackle";
        this.power = 2;
        this.apRequired = 1;
    }
}
